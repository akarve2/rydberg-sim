import numpy as np
from collections import defaultdict
from itertools import product
from math import prod
import timeit
import pathlib
import toml
import lib.plotdefs as pd

class ConfigStruct:
    pass

def config_fn(filename: str, subtab: str,
        props: list[(str, ..., type, str, type(lambda: ...))]) \
    -> (type(lambda: ConfigStruct), type(ConfigStruct)):
    """
    Returns a function and a class for reading and storing data from a TOML
    config file. Returned functions have the signature

    func(infile: pathlib.Path=pathlib.Path(`filename`))

    and the returned classes have attributes populated according to the elements
    of `props`, where each element is expected in the form

    (config_key, config_key_default, intype, attribute_name, processor)

    where `intype` is a Constructor taking a single argument for a type into
    which the value taken from the file is coerced, and `processor` is a
    function that transforms from the value taken from the file to the value
    ultimately stored in the Config object returned by `func`.

    Parameters
    ----------
    filename : str
        Path to default config file.
    subtab : str
        Key leading to the subtable in the config file storing relevant values.
    props : list[(str, ..., Constructor, str, Function)]
        Specification for processing relevant values.

    Returns
    -------
    func : Function
        Function for pulling and interpreting values from the config file.
    ConfigType : type
        Type for storing values pulled from the config file.
    """
    ConfigType = type(
        "Config",
        (ConfigStruct,),
        {field: proc(default) for _, default, _, field, proc in props}
    )

    def load_config(infile: pathlib.Path=pathlib.Path(filename)) -> ConfigType:
        table = toml.load(infile.open('r'))
        config = ConfigType()
        subtable = table.get(subtab, dict())
        for key, default, typecast, field, proc in props:
            X = typecast(subtable.get(key, default))
            setattr(config, field, proc(X))
        return config

    return (load_config, ConfigType)

def loop_call(func: type(lambda: ...), varies: dict[str, np.ndarray],
        fixes: dict[str, ...], printflag: bool=True, lspace: int=2) \
    -> list[np.ndarray]:
    N = [len(A) for A in varies.values()]
    Z = [int(np.log10(n)) + 1 for n in N]
    tot = prod(N)
    fmt = (
        lspace * " "
        + ";  ".join(f"{{:{z:.0f}.0f}} / {n:.0f}" for z, n in zip(Z, N))
        + ";  [{:6.2f}%] \r"
    )
    outputs = defaultdict(list)
    inputs = product(*[enumerate(A) for A in varies.values()])
    NN = [prod(N[-k:]) for k in range(1, len(N))][::-1] + [1]
    if printflag:
        print(
            fmt.format(*[1 for k in range(len(varies) - 1)], 0, 0.0),
            end="", flush=True
        )
    t0 = timeit.default_timer()
    for kX in inputs:
        K, X = zip(*kX)
        if printflag:
            print(
                fmt.format(
                    *[k + 1 for k in K],
                    100.0 * sum(k * nn for k, nn in zip(K, NN)) / tot
                ),
                end="", flush=True
            )
        Y = func(**dict(zip(varies.keys(), X)), **fixes)
        for j, y in enumerate(Y):
            outputs[j].append(y)
    T = timeit.default_timer() - t0
    if printflag:
        print(
            fmt.format(
                *[k + 1 for k in K],
                100.0 * (sum(k * nn for k, nn in zip(K, NN)) + 1) / tot
            ),
            flush=True
        )
        print(lspace * " " + f"total time elapsed: {T:.2f} s")
        print(lspace * " " + f"average time per call: {T / tot:.2f} s")
    return [np.array(y).reshape(tuple(N)) for y in outputs.values()]

def plot_bloch_sphere(a: np.complex128, b: np.complex128, k0: int,
        labels: list[str]) -> pd.Plotter:
    th = np.linspace(0, np.pi, 50)
    ph = np.linspace(0, 2 * np.pi, 50)
    TH, PH = np.meshgrid(th, ph)
    X = np.sin(TH) * np.cos(PH)
    Y = np.sin(TH) * np.sin(PH)
    Z = np.cos(TH)

    P = (pd.Plotter.new_3d(figsize=(2.25, 1.8))
        .plot_surface(X, Y, Z, alpha=0.25, color="#f0f0f0")
        .plot(np.cos(ph), np.sin(ph), np.zeros(ph.shape),
            color="#e8e8e8", linewidth=0.5)
        .plot(np.sin(ph), np.zeros(ph.shape), np.cos(ph),
            color="#e8e8e8", linewidth=0.5)
        .plot(np.zeros(ph.shape), np.sin(ph), np.cos(ph),
            color="#e8e8e8", linewidth=0.5)
        .plot([-1, 0], [0, 0], [0, 0], color="#e8e8e8", linestyle=":",
            linewidth=0.5)
        .plot([0, +1], [0, 0], [0, 0], color="#e8e8e8", linestyle="--",
            linewidth=0.5)
        .plot([0, 0], [-1, 0], [0, 0], color="#e8e8e8", linestyle=":",
            linewidth=0.5)
        .plot([0, 0], [0, +1], [0, 0], color="#e8e8e8", linestyle="--",
            linewidth=0.5)
        .plot([0, 0], [0, 0], [-1, 0], color="#e8e8e8", linestyle=":",
            linewidth=0.5)
        .plot([0, 0], [0, 0], [0, +1], color="#e8e8e8", linestyle="--",
            linewidth=0.5)
        .scatter([0], [0], [0], color="k", s=1)
        .text(0, 0, +1.1, labels[0],
            horizontalalignment="center",
            verticalalignment="bottom",
            fontsize="x-small"
        )
        .text(0, 0, -1.1, labels[1],
            horizontalalignment="center",
            verticalalignment="top",
            fontsize="x-small"
        )
        .set_box_aspect((1, 1, 1))
        .axis("off")
    )

    N = np.sqrt(abs(a)**2 + abs(b)**2)
    a_ = a / N
    b_ = b / N
    th_ = 2 * np.arccos(abs(a_))
    ph_ = np.arctan2(b_.imag, b_.real) - np.arctan2(a_.imag, a_.real)
    (P
        .plot(
            1.01 * np.sin(th_) * np.cos(ph_),
            1.01 * np.sin(th_) * np.sin(ph_),
            1.01 * np.cos(th_)
        )
        .scatter(
            1.01 * np.sin(th_[0]) * np.cos(ph_[0]),
            1.01 * np.sin(th_[0]) * np.sin(ph_[0]),
            1.01 * np.cos(th_[0]),
            color="g", s=0.5
        )
        .scatter(
            1.01 * np.sin(th_[k0]) * np.cos(ph_[k0]),
            1.01 * np.sin(th_[k0]) * np.sin(ph_[k0]),
            1.01 * np.cos(th_[k0]),
            color="0.65", s=0.5
        )
        .scatter(
            1.01 * np.sin(th_[-1]) * np.cos(ph_[-1]),
            1.01 * np.sin(th_[-1]) * np.sin(ph_[-1]),
            1.01 * np.cos(th_[-1]),
            color="r", s=0.5
        )
        .view_init(30, 30)
    )
    return P

