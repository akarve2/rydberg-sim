from __future__ import annotations
import numpy as np
import numpy.linalg as la
import numpy.random as rand

class Octave:
    """
    Holds the coefficients for interpolating cubic polynomials used in the
    generation of Perlin noise in a single octave. The entire assembly is
    defined over the [0, 1] interval.
    """
    def __init__(self, coeffs: np.ndarray):
        self.coeffs = coeffs

    @staticmethod
    def new(coeffs: np.ndarray):
        return Octave(coeffs)

    @staticmethod
    def from_gradients(g: np.ndarray):
        # map each consecutive pair of gradients to lie on either end of the
        # [0, 1] interval and find the coefficients for a cubic f such that:
        #   f(0) = f(1) = 0
        #   f'(0) = g0
        #   f'(1) = g1
        # where g0, g1 are the appropriate gradients
        coeffs = np.array([
            [g[k], -2 * g[k] - g[k + 1], g[k] + g[k + 1]]
            for k in range(len(g) - 1)
        ])
        return Octave(coeffs)

    def eval(self, n: int) -> np.ndarray:
        """
        Evaluate the octave at `n` evenly spaced gridpoints over its entire
        range.
        """
        X = np.linspace(0, 1, n)
        dX = 1 / self.coeffs.shape[0]
        subdivs = [
            X[np.where((X >= k * dX) * (X < (k + 1) * dX))]
            for k in range(self.coeffs.shape[0])
        ]
        nn = list()
        for k in range(self.coeffs.shape[0]):
            nn += list(
                self.coeffs[k, 0] * (x := (subdivs[k] - k * dX) / dX)
                + self.coeffs[k, 1] * x**2
                + self.coeffs[k, 2] * x**3
            )
        nn.append(self.coeffs[-1, :].sum())
        return np.array(nn)

    def eval_deriv(self, n: int) -> np.ndarray:
        """
        Evaluate the first derivative of the octave at `n` evenly spaced
        gridpoints over its entire range.
        """
        X = np.linspace(0, 1, n)
        dX = 1 / self.coeffs.shape[0]
        subdivs = [
            X[np.where((X >= k * dX) * (X < (k + 1) * dX))]
            for k in range(self.coeffs.shape[0])
        ]
        nn = list()
        for k in range(self.coeffs.shape[0]):
            nn += list(
                self.coeffs[k, 0]
                + 2 * self.coeffs[k, 1] * (x := (subdivs[k] - k * dX) / dX)
                + 3 * self.coeffs[k, 2] * x**2
            )
        nn.append(
            self.coeffs[-1, 0] + 2 * self.coeffs[-1, 1] + 3 * self.coeffs[-1, 2]
        )
        return np.array(nn)

class Noise:
    """
    Holds the data for generating a series of noise octaves and generating the
    overall signal. The entire assembly holds coefficients for signals defined
    over the [0, 1] interval.
    """
    def __init__(self, order: int, gradients: list[np.ndarray],
            octaves: list[Octave]):
        self.order = order
        self.gradients = gradients
        self.octaves = octaves

    @staticmethod
    def new(order: int=1, subdiv: int=2, std_g: float=1.0):
        """
        Construct by setting the number of octaves `order`, number of
        subdivisions `subdiv` in each octave, and standard deviation `std_g` of
        the (normal) distribution from which gradients are sampled.
        """
        gradients = [gen_gradients(subdiv**k + 1, std_g) for k in range(order)]
        octaves = [Octave.from_gradients(g) for g in gradients]
        return Noise(order, gradients, octaves)

    @staticmethod
    def from_gradients(gradients: list[np.ndarray]):
        """
        Construct from a list of arrays holding gradients, where each array
        corresponds to an octave.
        """
        order = len(G)
        octaves = [Octave.from_gradients(g) for g in gradients]
        return Noise(order, gradients, octaves)

    def sample(self, n: int, std: float=1.0, amp_scale: float=1.0):
        """
        Generate an array of Perlin noise at `n` gridpoints spaced evenly over
        the [0, 1] interval with successive octaves weighted relatively by
        `amp_scale` and the entire sum normalized such that the standard
        deviation of the resulting noise about zero is `std`.
        """
        nn = sum(amp_scale**k * O.eval(n) for k, O in enumerate(self.octaves))
        norm = std / np.sqrt((nn**2).mean())
        return norm * nn

def gen_gradients(N: int, std: float=1.0) -> np.ndarray:
    """
    Generate `N` gradients whose values are normally distributed about zero with
    standard deviation `std`.
    """
    return rand.normal(0.0, std, size=(N,))

def noise(n: int, std: float=1.0, order: int=1, amp_scale: float=1.0,
        subdiv: int=2, std_g: float=1.0) -> np.ndarray:
    """
    Generate an array of (`n` samples of) Perlin noise of `order` octaves, where
    successive octaves are weighted by relative amplitude `amp_scale` and
    generated from `subdiv`^k + 1 gradients, where k ranges from 0 to
    `order` - 1. The resulting array is normalized such that its standard
    deviation about zero is `std`.
    """
    return Noise(order, subdiv, std_g).sample(n, std, amp_scale)

