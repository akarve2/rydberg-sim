use ndarray::{
    self as nd,
    s,
};
use rand::{
    self as rnd,
    Rng,
    distributions::Distribution,
};
use statrs::{
    distribution as dist,
};

pub trait OctaveMethods {
    fn get_coeffs(&self) -> nd::ArrayView2<f64>;
    fn get_coeffs_mut(&mut self) -> nd::ArrayViewMut2<f64>;
    fn set_coeffs(&mut self, val: nd::Array2<f64>);

    fn eval(&self, n: usize) -> nd::Array1<f64> {
        let X: nd::Array1<f64> = nd::Array::linspace(0.0, 1.0, n);
        let dX: f64 = 1.0 / self.get_coeffs().shape()[0] as f64;
        let subdivs: Vec<nd::Array1<f64>> = (0..self.get_coeffs().shape()[0])
            .map(|k| nd::Array::from_iter(
                X.iter().filter_map(|&x| {
                    if x >= k as f64 * dX && x < (k + 1) as f64 * dX {
                        Some(x)
                    } else {
                        None
                    }
                })
            ))
            .collect();
        let mut nn: nd::Array1<f64> = nd::Array::zeros(n);
        let mut x: nd::Array1<f64>;
        let mut K: usize = 0;
        for (k, subdiv) in subdivs.iter().enumerate() {
            x = &(subdiv - (k as f64) * dX) / dX;
            nn.slice_mut(s![K..K + x.len()]).assign(&(
                self.get_coeffs()[[k, 0]] * &x
                + self.get_coeffs()[[k, 1]] * &x.mapv(|xi| xi.powi(2))
                + self.get_coeffs()[[k, 2]] * &x.mapv(|xi| xi.powi(3))
            ));
            K += x.len();
        }
        nn[n - 1] = self.get_coeffs()
            .slice(s![self.get_coeffs().shape()[0] - 1, ..])
            .sum();
        return nn;
    }

    fn eval_deriv(&self, n: usize) -> nd::Array1<f64> {
        let X: nd::Array1<f64> = nd::Array::linspace(0.0, 1.0, n);
        let dX: f64 = 1.0 / self.get_coeffs().shape()[0] as f64;
        let subdivs: Vec<nd::Array1<f64>> = (0..self.get_coeffs().shape()[0])
            .map(|k| nd::Array::from_iter(
                X.iter().filter_map(|&x| {
                    if x >= k as f64 * dX && x < (k + 1) as f64 * dX {
                        Some(x)
                    } else {
                        None
                    }
                })
            ))
            .collect();
        let mut nn: nd::Array1<f64> = nd::Array::zeros(n);
        let mut x: nd::Array1<f64>;
        let mut K: usize = 0;
        for (k, subdiv) in subdivs.iter().enumerate() {
            x = &(subdiv - (k as f64) * dX) / dX;
            nn.slice_mut(s![K..K + x.len()]).assign(&(
                self.get_coeffs()[[k, 0]]
                + 2.0 * self.get_coeffs()[[k, 1]] * &x
                + 3.0 * self.get_coeffs()[[k, 2]] * &x.mapv(|xi| xi.powi(2))
            ));
            K += x.len();
        }
        nn[n - 1]
            = self.get_coeffs()[[self.get_coeffs().shape()[0] - 1, 0]]
            + 2.0 * self.get_coeffs()[[self.get_coeffs().shape()[0] - 1, 1]]
            + 3.0 * self.get_coeffs()[[self.get_coeffs().shape()[0] - 1, 2]];
        return nn;
    }
}

#[derive(Clone, Default, Debug)]
pub struct Octave {
    pub coeffs: nd::Array2<f64>,
}

impl Octave {
    pub fn new(coeffs: nd::Array2<f64>) -> Self { Self { coeffs } }

    pub fn from_gradients(g: nd::ArrayView1<f64>) -> Self {
        // map each consecutive pair of gradients to lie on either end of the
        // [0, 1] interval and find the coefficients for a cubic f such that:
        //      f(0) = f(1) = 0
        //      f'(0) = g0
        //      f'(1) = g1
        // where g0, g1 are the appropriate gradients
        let coeffs: nd::Array2<f64> = nd::Array::from_shape_fn(
            (g.len() - 1, 3),
            |(i, j)| {
                if j == 0 {
                    g[i]
                } else if j == 1 {
                    -2.0 * g[i] - g[i + 1]
                } else {
                    g[i] + g[i + 1]
                }
            }
        );
        return Self { coeffs };
    }
}

impl OctaveMethods for Octave {
    fn get_coeffs(&self) -> nd::ArrayView2<f64> { self.coeffs.view() }

    fn get_coeffs_mut(&mut self) -> nd::ArrayViewMut2<f64> {
        return self.coeffs.view_mut()
    }

    fn set_coeffs(&mut self, val: nd::Array2<f64>) { self.coeffs = val; }
}

pub trait NoiseMethods<O>
where O: OctaveMethods
{
    fn get_order(&self) -> usize;
    fn set_order(&mut self, val: usize);

    fn get_gradients(&self) -> &Vec<nd::Array1<f64>>;
    fn get_gradients_mut(&mut self) -> &mut Vec<nd::Array1<f64>>;
    fn set_gradients(&mut self, val: Vec<nd::Array1<f64>>);

    fn get_octaves(&self) -> &Vec<O>;
    fn get_octaves_mut(&mut self) -> &mut Vec<O>;
    fn set_octaves(&mut self, val: Vec<O>);

    fn sample(&self, n: usize, std: f64, amp_scale: f64) -> nd::Array1<f64> {
        let mut nn: nd::Array1<f64> = nd::Array::zeros(n);
        for k in 0..self.get_octaves().len() {
            nn = &nn
                + &(amp_scale.powi(k as i32) * &self.get_octaves()[k].eval(n));
        }
        let norm: f64
            = std / (nn.mapv(|nni| nni.powi(2)).mean()).unwrap().sqrt();
        return norm * &nn;
    }
}

#[derive(Clone, Default, Debug)]
pub struct Noise {
    pub order: usize,
    pub gradients: Vec<nd::Array1<f64>>,
    pub octaves: Vec<Octave>,
}

impl Noise {
    pub fn new(order: usize, subdiv: usize, std_g: f64) -> Self {
        let mut rng: rnd::rngs::ThreadRng = rnd::thread_rng();
        let gradients: Vec<nd::Array1<f64>> = (0..order)
            .map(|k|
                gen_gradients_rng(subdiv.pow(k as u32) + 1, std_g, &mut rng)
            )
            .collect();
        let octaves: Vec<Octave> = gradients.iter()
            .map(|g| Octave::from_gradients(g.view()))
            .collect();
        return Self { order, gradients, octaves };
    }

    pub fn from_gradients(gradients: Vec<nd::Array1<f64>>) -> Self {
        let order: usize = gradients.len();
        let octaves: Vec<Octave> = gradients.iter()
            .map(|g| Octave::from_gradients(g.view()))
            .collect();
        return Self { order, gradients, octaves };
    }
}

impl NoiseMethods<Octave> for Noise {
    fn get_order(&self) -> usize { self.order }

    fn set_order(&mut self, val: usize) { self.order = val; }

    fn get_gradients(&self) -> &Vec<nd::Array1<f64>> { &self.gradients }

    fn get_gradients_mut(&mut self) -> &mut Vec<nd::Array1<f64>> {
        return &mut self.gradients;
    }

    fn set_gradients(&mut self, val: Vec<nd::Array1<f64>>) {
        self.gradients = val;
    }

    fn get_octaves(&self) -> &Vec<Octave> { &self.octaves }

    fn get_octaves_mut(&mut self) -> &mut Vec<Octave> { &mut self.octaves }

    fn set_octaves(&mut self, val: Vec<Octave>) { self.octaves = val; }
}

pub fn gen_gradients(n: usize, std_g: f64) -> nd::Array1<f64> {
    let mut rng: rnd::rngs::ThreadRng = rnd::thread_rng();
    return gen_gradients_rng(n, std_g, &mut rng);
}

pub fn gen_gradients_rng<R>(n: usize, std_g: f64, rng: &mut R) -> nd::Array1<f64>
    where R: Rng + ?Sized
{
    let normal: dist::Normal = dist::Normal::new(0.0, std_g).unwrap();
    return nd::Array::from_iter((0..n).map(|_| normal.sample(rng)));
}

pub fn noise(n: usize, std: f64, order: usize, amp_scale: f64, subdiv: usize,
             std_g: f64)
    -> nd::Array1<f64>
{
    return Noise::new(order, subdiv, std_g).sample(n, std, amp_scale);
}

