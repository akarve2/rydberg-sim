use pyo3::{
    prelude as pyo,
};
use numpy::{
    self as np,
    IntoPyArray,
};
use crate::{
    error::*,
    rabi::*,
    mkintopyerr,
};
use num_complex::Complex64 as C64;

mkintopyerr!(RabiError);

#[pyo::pymodule]
fn rabi(_py: pyo::Python, m: &pyo::PyModule) -> pyo::PyResult<()> {

    /// Compute the outer product of two 1D arrays,
    /// (a \otimes b)_{i,j} = a_i * (b^*)_j
    ///
    /// Parameters
    /// ----------
    /// a, b : numpy.ndarray[dim=1, dtype=numpy.complex128]
    ///
    /// Returns
    /// -------
    /// res : numpy.ndarray[dim=2, dtype=numpy.complex128]
    #[pyfn(m)]
    #[pyo3(
        name = "outer_prod",
        text_signature = "(a: np.ndarray, b: np.ndarray) -> np.ndarray"
    )]
    fn py_outer_prod(py: pyo::Python, a: &np::PyArray1<C64>,
                     b: &np::PyArray1<C64>)
        -> pyo::Py<np::PyArray2<C64>>
    {
        return outer_prod(
            &a.to_owned_array(),
            &b.to_owned_array(),
        ).into_pyarray(py).to_owned();
    }

    /// Compute [A, B] = A*B - B*A.
    #[pyfn(m)]
    #[pyo3(
        name = "commutator",
        text_signature = "(A: np.ndarray, B: np.ndarray) -> np.ndarray"
    )]
    fn py_commutator(py: pyo::Python, A: &np::PyArray2<C64>,
                     B: &np::PyArray2<C64>)
        -> pyo::Py<np::PyArray2<C64>>
    {
        return commutator(
            A.to_owned_array().view(),
            B.to_owned_array().view(),
        ).into_pyarray(py).to_owned();
    }

    /// Compute Tr(A) = \sum_i A_{i,i}.
    #[pyfn(m)]
    #[pyo3(
        name = "trace",
        text_signature = "(A: np.ndarray) -> np.complex128"
    )]
    fn py_trace(A: &np::PyArray2<C64>) -> C64 {
        return trace(&A.to_owned_array());
    }

    /// Compute the time-dependent behavior of an initial state by diagonalizing
    /// the Hamiltonian. Time-independent Hamiltonians only.
    ///
    /// Parameters
    /// ----------
    /// psi0 : numpy.ndarray[dim=1, dtype=numpy.complex128]
    ///     Initial state.
    /// H : numpy.ndarray[dim=2, dtype=numpy.complex128]
    ///     Hamiltonian (s^-1). Must be square, of shape compatible with `psi0`.
    /// t : numpy.ndarray[dim=1, dtype=numpy.float64]
    ///     Array of evenly spaced time gridpoints.
    ///
    /// Returns
    /// -------
    /// psi : numpy.ndarray[dim=2, dtype=numpy.complex128]
    ///     `psi0` evolved through time according to `H`. Has shape (I, J) where
    ///     I is equal to the length of `psi0` and J is equal to the length of
    ///     `t`.
    #[pyfn(m)]
    #[pyo3(
        name = "eigen_evolve",
        text_signature
            = "(psi0: np.ndarray, H: np.ndarray, t: np.ndarray) -> np.ndarray"
    )]
    fn py_eigen_evolve(py: pyo::Python, psi0: &np::PyArray1<C64>,
                       H: &np::PyArray2<C64>, t: &np::PyArray1<f64>)
        -> RabiResult<pyo::Py<np::PyArray2<C64>>>
    {
        return eigen_evolve(
            &psi0.to_owned_array(),
            &H.to_owned_array(),
            &t.to_owned_array(),
        )
        .map(|psi| psi.into_pyarray(py).to_owned());
    }

    /// Compute the time-dependent behavior of an initial state by diagonalizing
    /// the Hamiltonian at each time step.
    ///
    /// Parameters
    /// ----------
    /// psi0 : numpy.ndarray[dim=1, dtype=numpy.complex128]
    ///     Initial state.
    /// H : numpy.ndarray[dim=3, dtype=numpy.complex128]
    ///     Hamiltonian (in s^-1). The first two indices must correspond to
    ///     states (and be compatible with `psi0`) and the last must correspond
    ///     to time (and be compatible with `t`).
    /// t : numpy.ndarray[dim=1, dtype=numpy.float64]
    ///     Array of evenly spaced time gridpoints.
    ///
    /// Returns
    /// -------
    /// psi : numpy.ndarray[dim=2, dtype=numpy.complex128]
    ///     `psi0` evolved through time according to `H`. Has shape (I, J) where
    ///     I is equal to the length of `psi0` and J is equal to the length of
    ///     `t`.
    #[pyfn(m)]
    #[pyo3(
        name = "eigen_evolve_t",
        text_signature
            = "(psi0: np.ndarray, H: np.ndarray, t: np.ndarray) -> np.ndarray"
    )]
    fn py_eigen_evolve_t(py: pyo::Python, psi0: &np::PyArray1<C64>,
                         H: &np::PyArray3<C64>, t: &np::PyArray1<f64>)
        -> RabiResult<pyo::Py<np::PyArray2<C64>>>
    {
        return eigen_evolve_t(
            &psi0.to_owned_array(),
            &H.to_owned_array(),
            &t.to_owned_array(),
        )
        .map(|psi| psi.into_pyarray(py).to_owned());
    }

    /// Compute the time-dependent behavior of an initial state by numerically
    /// integrating the time-dependent Schrodinger equation using a simple
    /// central finite difference scheme (Error = O(dt^2)).
    ///
    /// Parameters
    /// ----------
    /// psi0 : numpy.ndarray[dim=1, dtype=numpy.complex128]
    ///     Initial state.
    /// H : numpy.ndarray[dim=2, dtype=numpy.complex128]
    ///     Hamiltonian (in s^-1). Must be square, of shape compatible with
    ///     `psi0`.
    /// t : numpy.ndarray[dim=1, dtype=numpy.float64]
    ///     Array of evenly spaced time gridpoints.
    ///
    /// Returns
    /// -------
    /// psi : numpy.ndarray[dim=2, dtype=numpy.complex128]
    ///     `psi0` evolved through time according to `H`. Has shape (I, J) where
    ///     I is equal to the length of `psi0` and J is equal to the length of
    ///     `t`.
    #[pyfn(m)]
    #[pyo3(
        name = "schrodinger_evolve",
        text_signature
            = "(psi0: np.ndarray, H: np.ndarray, t: np.ndarray) -> np.ndarray"
    )]
    fn py_schrodinger_evolve(py: pyo::Python, psi0: &np::PyArray1<C64>,
                             H: &np::PyArray2<C64>, t: &np::PyArray1<f64>)
        -> RabiResult<pyo::Py<np::PyArray2<C64>>>
    {
        return schrodinger_evolve(
            &psi0.to_owned_array(),
            &H.to_owned_array(),
            &t.to_owned_array(),
        )
        .map(|psi| psi.into_pyarray(py).to_owned());
    }

    /// Compute the time-dependent behavior of an initial state for a
    /// time-dependent Hamiltonian by numerically integrating the time-dependent
    /// Schrodinger equation using a simple central finite difference scheme
    /// (Error = O(dt^2)).
    ///
    /// Parameters
    /// ----------
    /// psi0 : numpy.ndarray[dim=1, dtype=numpy.complex128]
    ///     Initial state.
    /// H : numpy.ndarray[dim=3, dtype=numpy.complex128]
    ///     Hamiltonian (in s^-1). The first two indices must correspond to
    ///     states (and be compatible with `psi0`) and the last must correspond
    ///     to time (and be compatible with `t`).
    /// t : numpy.ndarray[dim=1, dtype=numpy.float64]
    ///     Array of evenly spaced time gridpoints.
    ///
    /// Returns
    /// -------
    /// psi : numpy.ndarray[dim=2, dtype=numpy.complex128]
    ///     `psi0` evolved through time according to `H`. Has shape (I, J) where
    ///     I is equal to the length of `psi0` and J is equal to the length of
    ///     `t`.
    #[pyfn(m)]
    #[pyo3(
        name = "schrodinger_evolve_t",
        text_signature
            = "(psi0: np.ndarray, H: np.ndarray, t: np.ndarray) -> np.ndarray"
    )]
    fn py_schrodinger_evolve_t(py: pyo::Python, psi0: &np::PyArray1<C64>,
                               H: &np::PyArray3<C64>, t: &np::PyArray1<f64>)
        -> RabiResult<pyo::Py<np::PyArray2<C64>>>
    {
        return schrodinger_evolve_t(
            &psi0.to_owned_array(),
            &H.to_owned_array(),
            &t.to_owned_array(),
        )
        .map(|psi| psi.into_pyarray(py).to_owned());
    }

    /// Compute the time-dependent behavior of an initial state for a
    /// time-dependent Hamiltonian by numerically integrating the time-dependent
    /// Schrodinger equation using fourth-order Runge-Kutta (Error = O(dt^5)).
    ///
    /// Parameters
    /// ----------
    /// psi0 : numpy.ndarray[dim=1, dtype=numpy.complex128]
    ///     Initial state.
    /// H : numpy.ndarray[dim=3, dtype=numpy.complex128]
    ///     Hamiltonian (in s^-1). The first two indices must correspond to
    ///     states (and be compatible with `psi0`) and the last must correspond
    ///     to time (and be compatible with `t`).
    /// t : numpy.ndarray[dim=1, dtype=numpy.float64]
    ///     Array of evenly spaced time gridpoints.
    ///
    /// Returns
    /// -------
    /// psi : numpy.ndarray[dim=2, dtype=numpy.complex128]
    ///     `psi0` evolved through time according to `H`. Has shape (I, J) where
    ///     I is equal to the length of `psi0` and J is equal to the length of
    ///     `t`.
    #[pyfn(m)]
    #[pyo3(
        name = "schrodinger_evolve_rk4",
        text_signature
            = "(psi0: np.ndarray, H: np.ndarray, t: np.ndarray) -> np.ndarray"
    )]
    fn py_schrodinger_evolve_rk4(py: pyo::Python, psi0: &np::PyArray1<C64>,
                                 H: &np::PyArray3<C64>, t: &np::PyArray1<f64>)
        -> RabiResult<pyo::Py<np::PyArray2<C64>>>
    {
        return schrodinger_evolve_rk4(
            &psi0.to_owned_array(),
            &H.to_owned_array(),
            &t.to_owned_array(),
        )
        .map(|psi| psi.into_pyarray(py).to_owned());
    }

    /// Compute the time-dependent behavior of an initial state by numerically
    /// integrating the Liouville equation using a simple central finite
    /// difference scheme (Error = O(dt^2)).
    ///
    /// Parameters
    /// ----------
    /// psi0 : numpy.ndarray[dim=1, dtype=numpy.complex128]
    ///     Initial state.
    /// H : numpy.ndarray[dim=2, dtype=numpy.complex128]
    ///     Hamiltonian (in s^-1). Must be square, of shape compatible with
    ///     `psi0`.
    /// t : numpy.ndarray[dim=1, dtype=numpy.float64]
    ///     Array of evenly spaced time gridpoints.
    ///
    /// Returns
    /// -------
    /// rho : numpy.ndarray[dim=3, dtype=numpy.complex128]
    ///     Density matrix of `psi0` evolved through time according to `H`. Has
    ///     shape (I, J, K) where I and J are equal to the length of `psi0` and
    ///     K is equal to the length of `t`.
    #[pyfn(m)]
    #[pyo3(
        name = "liouville_evolve",
        text_signature
            = "(psi0: np.ndarray, H: np.ndarray, t: np.ndarray) -> np.ndarray"
    )]
    fn py_liouville_evolve(py: pyo::Python, psi0: &np::PyArray1<C64>,
                           H: &np::PyArray2<C64>, t: &np::PyArray1<f64>)
        -> RabiResult<pyo::Py<np::PyArray3<C64>>>
    {
        return liouville_evolve(
            &psi0.to_owned_array(),
            &H.to_owned_array(),
            &t.to_owned_array(),
        )
        .map(|rho| rho.into_pyarray(py).to_owned());
    }

    /// Compute the time-dependent behavior of an initial state for a
    /// time-dependent Hamiltonian by numerically integrating the Liouville
    /// equation using a simple central finite difference scheme 
    /// (Error = O(dt^2)).
    ///
    /// Parameters
    /// ----------
    /// psi0 : numpy.ndarray[dim=1, dtype=numpy.complex128]
    ///     Initial state.
    /// H : numpy.ndarray[dim=3, dtype=numpy.complex128]
    ///     Hamiltonian (in s^-1). The first two indices must correspond to
    ///     states (and be compatible with `psi0`) and the last must correspond
    ///     to time (and be compatible with `t`).
    /// t : numpy.ndarray[dim=1, dtype=numpy.float64]
    ///     Array of evenly spaced time gridpoints.
    ///
    /// Returns
    /// -------
    /// rho : numpy.ndarray[dim=3, dtype=numpy.complex128]
    ///     Density matrix of `psi0` evolved through time according to `H`. Has
    ///     shape (I, J, K) where I and J are equal to the length of `psi0` and
    ///     K is equal to the length of `t`.
    #[pyfn(m)]
    #[pyo3(
        name = "liouville_evolve_t",
        text_signature
            = "(psi0: np.ndarray, H: np.ndarray, t: np.ndarray) -> np.ndarray"
    )]
    fn py_liouville_evolve_t(py: pyo::Python, psi0: &np::PyArray1<C64>,
                             H: &np::PyArray3<C64>, t: &np::PyArray1<f64>)
        -> RabiResult<pyo::Py<np::PyArray3<C64>>>
    {
        return liouville_evolve_t(
            &psi0.to_owned_array(),
            &H.to_owned_array(),
            &t.to_owned_array(),
        )
        .map(|rho| rho.into_pyarray(py).to_owned());
    }

    /// Compute the time-dependent behavior of an initial state for a
    /// time-dependent Hamiltonian by numerically integrating the Liouville
    /// equation using fourth-order Runge-Kutta (Error = O(dt^5)).
    ///
    /// Parameters
    /// ----------
    /// psi0 : numpy.ndarray[dim=1, dtype=numpy.complex128]
    ///     Initial state.
    /// H : numpy.ndarray[dim=3, dtype=numpy.complex128]
    ///     Hamiltonian (in s^-1). The first two indices must correspond to
    ///     states (and be compatible with `psi0`) and the last must correspond
    ///     to time (and be compatible with `t`).
    /// t : numpy.ndarray[dim=1, dtype=numpy.float64]
    ///     Array of evenly spaced time gridpoints.
    ///
    /// Returns
    /// -------
    /// rho : numpy.ndarray[dim=3, dtype=numpy.complex128]
    ///     Density matrix of `psi0` evolved through time according to `H`. Has
    ///     shape (I, J, K) where I and J are equal to the length of `psi0` and
    ///     K is equal to the length of `t`.
    #[pyfn(m)]
    #[pyo3(
        name = "liouville_evolve_rk4",
        text_signature
            = "(psi0: np.ndarray, H: np.ndarray, t: np.ndarray) -> np.ndarray"
    )]
    fn py_liouville_evolve_rk4(py: pyo::Python, psi0: &np::PyArray1<C64>,
                             H: &np::PyArray3<C64>, t: &np::PyArray1<f64>)
        -> RabiResult<pyo::Py<np::PyArray3<C64>>>
    {
        return liouville_evolve_rk4(
            &psi0.to_owned_array(),
            &H.to_owned_array(),
            &t.to_owned_array(),
        )
        .map(|rho| rho.into_pyarray(py).to_owned());
    }

    /// Compute the time-dependent behavior of an initial state by numerically
    /// integrating the Lindblad equation using a simple central finite
    /// difference scheme (Error = O(dt^2)).
    ///
    /// Parameters
    /// ----------
    /// psi0 : numpy.ndarray[dim=1, dtype=numpy.complex128]
    ///     Initial state.
    /// H : numpy.ndarray[dim=2, dtype=numpy.complex128]
    ///     Hamiltonian (in s^-1). Must be square, of shape compatible with
    ///     `psi0`.
    /// Y : numpy.ndarray[dim=2, dtype=numpy.float64]
    ///     Matrix providing decay-rate couplings (in s^-1). Must be square, of
    ///     shape compatible with `psi0`.
    /// t : numpy.ndarray[dim=1, dtype=numpy.float64]
    ///     Array of evenly spaced time gridpoints.
    ///
    /// Returns
    /// -------
    /// rho : numpy.ndarray[dim=3, dtype=numpy.complex128]
    ///     Density matrix of `psi0` evolved through time according to `H`. Has
    ///     shape (I, J, K) where I and J are equal to the length of `psi0` and
    ///     K is equal to the length of `t`.
    #[pyfn(m)]
    #[pyo3(
        name = "lindblad_evolve",
        text_signature
            = "(psi0: np.ndarray, H: np.ndarray, Y: np.ndarray, t: np.ndarray) -> np.ndarray"
    )]
    fn py_lindblad_evolve(py: pyo::Python, psi0: &np::PyArray1<C64>,
                           H: &np::PyArray2<C64>, Y: &np::PyArray2<f64>,
                           t: &np::PyArray1<f64>)
        -> RabiResult<pyo::Py<np::PyArray3<C64>>>
    {
        return lindblad_evolve(
            &psi0.to_owned_array(),
            &H.to_owned_array(),
            &Y.to_owned_array(),
            &t.to_owned_array(),
        )
        .map(|rho| rho.into_pyarray(py).to_owned());
    }

    /// Compute the time-dependent behavior of an initial state for a
    /// time-dependent Hamiltonian by numerically integrating the Lindblad
    /// equation using a simple central finite difference scheme 
    /// (Error = O(dt^2)).
    ///
    /// Parameters
    /// ----------
    /// psi0 : numpy.ndarray[dim=1, dtype=numpy.complex128]
    ///     Initial state.
    /// H : numpy.ndarray[dim=3, dtype=numpy.complex128]
    ///     Hamiltonian (in s^-1). The first two indices must correspond to
    ///     states (and be compatible with `psi0`) and the last must correspond
    ///     to time (and be compatible with `t`).
    /// Y : numpy.ndarray[dim=2, dtype=numpy.float64]
    ///     Matrix providing decay-rate couplings (in s^-1). Must be square, of
    ///     shape compatible with `psi0`.
    /// t : numpy.ndarray[dim=1, dtype=numpy.float64]
    ///     Array of evenly spaced time gridpoints.
    ///
    /// Returns
    /// -------
    /// rho : numpy.ndarray[dim=3, dtype=numpy.complex128]
    ///     Density matrix of `psi0` evolved through time according to `H`. Has
    ///     shape (I, J, K) where I and J are equal to the length of `psi0` and
    ///     K is equal to the length of `t`.
    #[pyfn(m)]
    #[pyo3(
        name = "lindblad_evolve_t",
        text_signature
            = "(psi0: np.ndarray, H: np.ndarray, Y: np.ndarray, t: np.ndarray) -> np.ndarray"
    )]
    fn py_lindblad_evolve_t(py: pyo::Python, psi0: &np::PyArray1<C64>,
                             H: &np::PyArray3<C64>, Y: &np::PyArray2<f64>,
                             t: &np::PyArray1<f64>)
        -> RabiResult<pyo::Py<np::PyArray3<C64>>>
    {
        return lindblad_evolve_t(
            &psi0.to_owned_array(),
            &H.to_owned_array(),
            &Y.to_owned_array(),
            &t.to_owned_array(),
        )
        .map(|rho| rho.into_pyarray(py).to_owned());
    }

    /// Compute the time-dependent behavior of an initial state for a
    /// time-dependent Hamiltonian by numerically integrating the Lindblad
    /// equation using fourth-order Runge-Kutta (Error = O(dt^5)).
    ///
    /// Parameters
    /// ----------
    /// psi0 : numpy.ndarray[dim=1, dtype=numpy.complex128]
    ///     Initial state.
    /// H : numpy.ndarray[dim=3, dtype=numpy.complex128]
    ///     Hamiltonian (in s^-1). The first two indices must correspond to
    ///     states (and be compatible with `psi0`) and the last must correspond
    ///     to time (and be compatible with `t`).
    /// Y : numpy.ndarray[dim=2, dtype=numpy.float64]
    ///     Matrix providing decay-rate couplings (in s^-1). Must be square, of
    ///     shape compatible with `psi0`.
    /// t : numpy.ndarray[dim=1, dtype=numpy.float64]
    ///     Array of evenly spaced time gridpoints.
    ///
    /// Returns
    /// -------
    /// rho : numpy.ndarray[dim=3, dtype=numpy.complex128]
    ///     Density matrix of `psi0` evolved through time according to `H`. Has
    ///     shape (I, J, K) where I and J are equal to the length of `psi0` and
    ///     K is equal to the length of `t`.
    #[pyfn(m)]
    #[pyo3(
        name = "lindblad_evolve_rk4",
        text_signature
            = "(psi0: np.ndarray, H: np.ndarray, Y: np.ndarray, t: np.ndarray) -> np.ndarray"
    )]
    fn py_lindblad_evolve_rk4(py: pyo::Python, psi0: &np::PyArray1<C64>,
                             H: &np::PyArray3<C64>, Y: &np::PyArray2<f64>,
                             t: &np::PyArray1<f64>)
        -> RabiResult<pyo::Py<np::PyArray3<C64>>>
    {
        return lindblad_evolve_rk4(
            &psi0.to_owned_array(),
            &H.to_owned_array(),
            &Y.to_owned_array(),
            &t.to_owned_array(),
        )
        .map(|rho| rho.into_pyarray(py).to_owned());
    }

    /// Find the index of the `M`-th local maximum in an array of oscillating
    /// values.
    #[pyfn(m,
        M="1",
        eps="1e-3",
    )]
    #[pyo3(
        name = "find_2pi_idx",
        text_signature = "(X: np.ndarray, M: int=1, eps: float=1e-3)"
    )]
    fn py_find_2pi_idx(X: &np::PyArray1<f64>, M: usize, eps: f64)
        -> (usize, f64)
    {
        return find_2pi_idx(&X.to_owned_array(), M, eps);
    }

    return Ok(());
}

