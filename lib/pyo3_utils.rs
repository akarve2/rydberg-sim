use std::{
    error,
    fmt,
};
use pyo3::{
    types as pytypes,
    conversion as pyconv,
};

pub fn pydict_get_opt<'a, T, K, E>(dict: &&'a pytypes::PyDict, key: K, err: E)
    -> Result<Option<T>, E>
    where T: pyconv::FromPyObject<'a>,
          K: pyconv::ToBorrowedObject,
          E: error::Error + fmt::Display,
{
    let res: Option<T> = match dict.get_item(key) {
        Some(Obj) => Some(Obj.extract().map_err(|_err| err)?),
        None => None,
    };
    return Ok(res);
}

pub fn pydict_get_nec<'a, T, K, E>
    (dict: &&'a pytypes::PyDict, key: K, missing_err: E, conv_err: E)
    -> Result<T, E>
    where T: pyconv::FromPyObject<'a>,
          K: pyconv::ToBorrowedObject,
          E: error::Error + fmt::Display,
{
    let res: Result<T, E> = match dict.get_item(key) {
        Some(Obj) => Ok(Obj.extract().map_err(|_err| conv_err)?),
        None => Err(missing_err),
    };
    return res;
}

pub trait GetExtract {
    fn get_extract<'a, T, K, E>(&'a self, key: K, missing_err: E, conv_err: E)
        -> Result<T, E>
        where T: pyconv::FromPyObject<'a>,
              K: pyconv::ToBorrowedObject,
              E: error::Error + fmt::Display;

    fn get_extract_opt<'a, T, K, E>(&'a self, key: K, conv_err: E)
        -> Result<Option<T>, E>
        where T: pyconv::FromPyObject<'a>,
              K: pyconv::ToBorrowedObject,
              E: error::Error + fmt::Display;
}

impl GetExtract for pytypes::PyDict {
    fn get_extract<'a, T, K, E>(&'a self, key: K, missing_err: E, conv_err: E)
        -> Result<T, E>
        where T: pyconv::FromPyObject<'a>,
              K: pyconv::ToBorrowedObject,
              E: error::Error + fmt::Display,
    {
        let res: Result<T, E> = match self.get_item(key) {
            Some(Obj) => Ok(Obj.extract().map_err(|_err| conv_err)?),
            None => Err(missing_err),
        };
        return res;
    }

    fn get_extract_opt<'a, T, K, E>(&'a self, key: K, conv_err: E)
        -> Result<Option<T>, E>
        where T: pyconv::FromPyObject<'a>,
              K: pyconv::ToBorrowedObject,
              E: error::Error + fmt::Display,
    {
        let res: Option<T> = match self.get_item(key) {
            Some(Obj) => Some(Obj.extract().map_err(|_err| conv_err)?),
            None => None,
        };
        return Ok(res);
    }
}

#[macro_export]
macro_rules! mkintopyerr {
    ( $name:ident ) => {
        impl From<$name> for pyo3::prelude::PyErr {
            fn from(err: $name) -> Self {
                return pyo3::exceptions::PyException::new_err(err.msg());
            }
        }
    }
}

