//! Functions for numerical integration of the Schrődinger and Lindblad master
//! equations using a variety of methods.
//!
//! Where unspecified, the last index of an array corresponds to time.

use ndarray::{
    self as nd,
    s,
};
use ndarray_linalg::{
    self as la,
    Eigh,
    Solve,
};
use num_complex::Complex64 as C64;
use crate::{
    error::*,
};

/// Compute the outer product `a b*`.
pub fn outer_prod(a: &nd::Array1<C64>, b: &nd::Array1<C64>) -> nd::Array2<C64> {
    return nd::Array2::from_shape_fn(
        (a.len(), b.len()), |(i, j)| a[i] * b[j].conj());
}

/// Compute the commutator `[A, B] = A B - B A`.
pub fn commutator(A: nd::ArrayView2<C64>, B: nd::ArrayView2<C64>)
    -> nd::Array2<C64>
{
    return A.dot(&B) - B.dot(&A);
}

/// Compute the anti-commutator `{A, B} = A B + B A`
pub fn anti_commutator(A: nd::ArrayView2<C64>, B: nd::ArrayView2<C64>)
    -> nd::Array2<C64>
{
    return A.dot(&B) + B.dot(&A);
}

/// Compute the non-hermitian part of the RHS of the Lindblad master equation.
pub fn lindbladian(Y: nd::ArrayView2<f64>, rho: nd::ArrayView2<C64>)
    -> nd::Array2<C64>
{
    let n: usize = Y.raw_dim()[0];
    let mut L: nd::Array2<C64> = nd::Array2::zeros(Y.raw_dim());
    let mut M: nd::Array2<C64>;
    for ((a, b), &y) in Y.indexed_iter() {
        if y <= f64::EPSILON { continue; }
        M = nd::Array2::from_shape_fn(
            (n, n),
            |(i, j)| { y * (
                if i == a && j == a { rho[[b, b]] } else { C64::new(0.0, 0.0) }
                - if i == b { rho[[i, j]] / 2.0 } else { C64::new(0.0, 0.0) }
                - if j == b { rho[[i, j]] / 2.0 } else { C64::new(0.0, 0.0) }
            ) }
        );
        L += &M;
    }
    return L;
}

/// Compute the trace of a square matrix `A`.
pub fn trace(A: &nd::Array2<C64>) -> C64 {
    assert!(A.is_square());
    return (0..A.shape()[0]).map(|k| A[[k, k]]).sum();
}

/// Compute the Schrődinger coherent evolution of the initial state `psi0` for
/// Hamiltonian `H`.
///
/// Requires `H` to be in units of s^-1.
pub fn eigen_evolve(psi0: &nd::Array1<C64>, H: &nd::Array2<C64>,
                    t: &nd::Array1<f64>)
    -> RabiResult<nd::Array2<C64>>
{
    let (E, V): (nd::Array1<f64>, nd::Array2<C64>)
        = H.eigh(la::UPLO::Lower).map_err(|_| RabiError::Eigen)?;
    let c: nd::Array1<C64> = V.solve(psi0).map_err(|_| RabiError::Linalg)?;
    let mut psi: nd::Array2<C64> = nd::Array::zeros((psi0.len(), t.len()));
    for k in 0..t.len() {
        V.dot(&(&c * &E.mapv(|e| (-C64::i() * e * t[k]).exp())))
            .move_into(psi.slice_mut(s![.., k]));
    }
    return Ok(psi);
}

/// Compute the Schrődinger coherent evolution of the initial state `psi0` for
/// time-dependent Hamiltonian `H` by diagonalizing at each time step.
///
/// Requires `H` to be in units of s^-1. The third index of `H` corresponds to
/// time.
pub fn eigen_evolve_t(psi0: &nd::Array1<C64>, H: &nd::Array3<C64>,
                      t: &nd::Array1<f64>)
    -> RabiResult<nd::Array2<C64>>
{
    let dt: f64 = (t[1] - t[0]).abs();
    let mut EV: (nd::Array1<f64>, nd::Array2<C64>);
    let mut c: nd::Array1<C64>;
    let mut psi: nd::Array2<C64> = nd::Array::zeros((psi0.len(), t.len()));
    let mut psi_new: nd::Array1<C64>;
    psi.slice_mut(s![.., 0]).assign(psi0);
    for k in 0..t.len() - 1 {
        EV = H.slice(s![.., .., k]).eigh(la::UPLO::Lower)
            .map_err(|_| RabiError::Eigen)?;
        c = EV.1.solve(&psi.slice(s![.., k])).map_err(|_| RabiError::Linalg)?;
        psi_new = EV.1.dot(&(c * EV.0.mapv(|e| (-C64::i() * e * dt).exp())));
        psi_new.move_into(psi.slice_mut(s![.., k + 1]));
    }
    return Ok(psi);
}

/// Numerically integrate the Schrődinger equation using the midpoint rule.
///
/// Requires `H` to be in units of s^-1.
pub fn schrodinger_evolve(psi0: &nd::Array1<C64>, H: &nd::Array2<C64>,
                          t: &nd::Array1<f64>)
    -> RabiResult<nd::Array2<C64>>
{
    let dt: f64 = (t[1] - t[0]).abs();
    let mut psi: nd::Array2<C64> = nd::Array::zeros((psi0.len(), t.len()));
    let mut dpsi: nd::Array1<C64>;
    let mut psi_new: nd::Array1<C64>;
    let mut N: C64;
    psi.slice_mut(s![.., 0]).assign(psi0);
    psi.slice_mut(s![.., 1]).assign(psi0);
    for k in 1..t.len() - 1 {
        dpsi = (-2.0 * dt * C64::i()) * H.dot(&psi.slice(s![.., k]));
        psi_new = &psi.slice(s![.., k - 1]) + dpsi;
        N = psi_new.mapv(|a| a * a.conj()).sum().sqrt();
        (psi_new / N).move_into(psi.slice_mut(s![.., k + 1]));
        // psi.slice_mut(s![.., k + 1]).assign(&(&psi_new / N));
    }
    return Ok(psi);
}

/// Numerically integrate the Schrődinger equation using the midpoint rule for a
/// time-dependent Hamiltonian.
///
/// Requires `H` to be in units of s^-1. The third index of `H` corresponds to
/// time.
pub fn schrodinger_evolve_t(psi0: &nd::Array1<C64>, H: &nd::Array3<C64>,
                            t: &nd::Array1<f64>)
    -> RabiResult<nd::Array2<C64>>
{
    let dt: f64 = (t[1] - t[0]).abs();
    let mut psi: nd::Array2<C64> = nd::Array::zeros((psi0.len(), t.len()));
    let mut dpsi: nd::Array1<C64>;
    let mut psi_new: nd::Array1<C64>;
    let mut N: C64;
    psi.slice_mut(s![.., 0]).assign(psi0);
    psi.slice_mut(s![.., 1]).assign(psi0);
    for k in 1..t.len() - 1 {
        dpsi = (-2.0 * dt * C64::i())
            * H.slice(s![.., .., k]).dot(&psi.slice(s![.., k]));
        psi_new = &psi.slice(s![.., k - 1]) + dpsi;
        N = psi_new.mapv(|a| a * a.conj()).sum().sqrt();
        (psi_new / N).move_into(psi.slice_mut(s![.., k + 1]));
        // psi.slice_mut(s![.., k + 1]).assign(&(&psi_new / N));
    }
    return Ok(psi);
}

/// Numerically integrate the Schrődinger equation using fourth-order
/// Runge-Kutta for a time-dependent Hamiltonian.
///
/// Requires `H` to be in units of s^-1. The third index of `H` corresponds to
/// time.
pub fn schrodinger_evolve_rk4(psi0: &nd::Array1<C64>, H: &nd::Array3<C64>,
                              t: &nd::Array1<f64>)
    -> RabiResult<nd::Array2<C64>>
{
    let dt: f64 = (t[1] - t[0]).abs();
    let mut psi: nd::Array2<C64> = nd::Array::zeros((psi0.len(), t.len()));
    let (mut phi1, mut phi2, mut phi3, mut phi4):
        (nd::Array1<C64>, nd::Array1<C64>, nd::Array1<C64>, nd::Array1<C64>);
    let mut psi_old: nd::ArrayView1<C64>;
    let mut psi_new: nd::Array1<C64>;
    let mut N: C64;
    psi.slice_mut(s![.., 0]).assign(psi0);
    psi.slice_mut(s![.., 1]).assign(psi0);
    let rhs = |H: nd::ArrayView2<C64>, p: nd::ArrayView1<C64>|
        -C64::i() * H.dot(&p);
    for k in 0..t.len() - 2 {
        psi_old = psi.slice(s![.., k]);
        phi1 = rhs(H.slice(s![.., .., k]), psi_old);
        phi2 = rhs(
            H.slice(s![.., .., k + 1]),
            (&psi_old + phi1.mapv(|a| dt * a)).view()
        );
        phi3 = rhs(
            H.slice(s![.., .., k + 1]),
            (&psi_old + phi2.mapv(|a| dt * a)).view()
        );
        phi4 = rhs(
            H.slice(s![.., .., k + 2]),
            (&psi_old + phi3.mapv(|a| 2.0 * dt * a)).view()
        );

        psi_new = &psi_old
            + (
                phi1 + phi2.mapv(|a| 2.0 * a)
                + phi3.mapv(|a| 2.0 * a) + phi4
            ).mapv(|a| dt / 3.0 * a);
        N = psi_new.mapv(|a| a * a.conj()).sum().sqrt();

        (psi_new / N).move_into(psi.slice_mut(s![.., k + 2]));
        // psi.slice_mut(s![.., k + 2]).assign(&(&psi_new / N));
    }
    return Ok(psi);
}

/// Numerically integrate the Liouville equation using the midpoint rule.
///
/// Requires `H` to be in units of s^-1.
pub fn liouville_evolve(psi0: &nd::Array1<C64>, H: &nd::Array2<C64>,
                        t: &nd::Array1<f64>)
    -> RabiResult<nd::Array3<C64>>
{
    let dt: f64 = (t[1] - t[0]).abs();
    let rho0: nd::Array2<C64> = outer_prod(psi0, psi0);
    let mut rho: nd::Array3<C64>
        = nd::Array::zeros((psi0.len(), psi0.len(), t.len()));
    let mut drho: nd::Array2<C64>;
    let mut rho_new: nd::Array2<C64>;
    let mut N: C64;
    rho.slice_mut(s![.., .., 0]).assign(&rho0);
    rho.slice_mut(s![.., .., 1]).assign(&rho0);
    for k in 1..t.len() - 1 {
        drho = (-2.0 * dt * C64::i())
            * commutator(H.view(), rho.slice(s![.., .., k]));
        rho_new = &rho.slice(s![.., .., k - 1]) + drho;
        N = trace(&rho_new);
        (rho_new / N).move_into(rho.slice_mut(s![.., .., k + 1]));
        // rho.slice_mut(s![.., .., k + 1]).assign(&(&rho_new / N));
    }
    return Ok(rho);
}

/// Numerically integrate the Liouville equation using the midpoint rule for a
/// time-dependent Hamiltonian.
///
/// Requires `H` to be in units of s^-1. The third index of `H` corresponds to
/// time.
pub fn liouville_evolve_t(psi0: &nd::Array1<C64>, H: &nd::Array3<C64>,
                          t: &nd::Array1<f64>)
    -> RabiResult<nd::Array3<C64>>
{
    let dt: f64 = (t[1] - t[0]).abs();
    let rho0: nd::Array2<C64> = outer_prod(psi0, psi0);
    let mut rho: nd::Array3<C64>
        = nd::Array::zeros((psi0.len(), psi0.len(), t.len()));
    let mut drho: nd::Array2<C64>;
    let mut rho_new: nd::Array2<C64>;
    let mut N: C64;
    rho.slice_mut(s![.., .., 0]).assign(&rho0);
    rho.slice_mut(s![.., .., 1]).assign(&rho0);
    for k in 1..t.len() - 1 {
        drho = (-2.0 * dt * C64::i())
            * commutator(H.slice(s![.., .., k]), rho.slice(s![.., .., k]));
        rho_new = &rho.slice(s![.., .., k - 1]) + drho;
        N = trace(&rho_new);
        (rho_new / N).move_into(rho.slice_mut(s![.., .., k + 1]));
        // rho.slice_mut(s![.., .., k + 1]).assign(&(&rho_new / N));
    }
    return Ok(rho);
}

/// Numerically integrate the Liouville equation using fourth-order Runge-Kutta
/// for a time-dependent Hamiltonian.
///
/// Requires `H` to be in units of s^-1. The third index of `H` corresponds to
/// time.
pub fn liouville_evolve_rk4(psi0: &nd::Array1<C64>, H: &nd::Array3<C64>,
                            t: &nd::Array1<f64>)
    -> RabiResult<nd::Array3<C64>>
{
    let dt: f64 = (t[1] - t[0]).abs();
    let rho0: nd::Array2<C64> = outer_prod(psi0, psi0);
    let mut rho: nd::Array3<C64>
        = nd::Array::zeros((psi0.len(), psi0.len(), t.len()));
    let (mut r1, mut r2, mut r3, mut r4):
        (nd::Array2<C64>, nd::Array2<C64>, nd::Array2<C64>, nd::Array2<C64>);
    let mut rho_old: nd::ArrayView2<C64>;
    let mut rho_new: nd::Array2<C64>;
    let mut N: C64;
    rho.slice_mut(s![.., .., 0]).assign(&rho0);
    rho.slice_mut(s![.., .., 1]).assign(&rho0);
    let rhs = |H: nd::ArrayView2<C64>, p: nd::ArrayView2<C64>|
        -C64::i() * commutator(H, p);
    for k in 0..t.len() - 2 {
        rho_old = rho.slice(s![.., .., k]);
        r1 = rhs(H.slice(s![.., .., k]), rho_old);
        r2 = rhs(
            H.slice(s![.., .., k + 1]),
            (&rho_old + r1.mapv(|r| dt * r)).view()
        );
        r3 = rhs(
            H.slice(s![.., .., k + 1]),
            (&rho_old + r2.mapv(|r| dt * r)).view()
        );
        r4 = rhs(
            H.slice(s![.., .., k + 2]),
            (&rho_old + r3.mapv(|r| 2.0 * dt * r)).view()
        );

        rho_new = &rho_old
            + (
                r1 + r2.mapv(|r| 2.0 * r) + r3.mapv(|r| 2.0 * r) + r4
            ).mapv(|r| dt / 3.0 * r);
        N = trace(&rho_new);

        (rho_new / N).move_into(rho.slice_mut(s![.., .., k + 2]));
        // rho.slice_mut(s![.., .., k + 2]).assign(&(&rho_new / N));
    }
    return Ok(rho);
}

/// Numerically integrate the Lindblad equation using the midpoint rule.
///
/// Requires `H` and `Y` to be in units of s^-1.
pub fn lindblad_evolve(psi0: &nd::Array1<C64>, H: &nd::Array2<C64>,
                       Y: &nd::Array2<f64>, t: &nd::Array1<f64>)
    -> RabiResult<nd::Array3<C64>>
{
    let dt: f64 = (t[1] - t[0]).abs();
    let rho0: nd::Array2<C64> = outer_prod(psi0, psi0);
    let mut rho: nd::Array3<C64>
        = nd::Array::zeros((psi0.len(), psi0.len(), t.len()));
    let mut drho: nd::Array2<C64>;
    let mut rho_new: nd::Array2<C64>;
    let mut N: C64;
    rho.slice_mut(s![.., .., 0]).assign(&rho0);
    rho.slice_mut(s![.., .., 1]).assign(&rho0);
    for k in 1..t.len() - 1 {
        drho = C64::from(2.0 * dt)
            * (
                -C64::i() * commutator(H.view(), rho.slice(s![.., .., k]))
                + lindbladian(Y.view(), rho.slice(s![.., .., k]))
            );
        rho_new = &rho.slice(s![.., .., k - 1]) + drho;
        N = trace(&rho_new);
        (rho_new / N).move_into(rho.slice_mut(s![.., .., k + 1]));
    }
    return Ok(rho);
}

/// Numerically integrate the Lindblad equation using the midpoint rule for a
/// time-dependent Hamiltonian.
///
/// Requires `H` and `Y` to be in units of s^-1. The third index of `H`
/// corresponds to time.
pub fn lindblad_evolve_t(psi0: &nd::Array1<C64>, H: &nd::Array3<C64>,
                         Y: &nd::Array2<f64>, t: &nd::Array1<f64>)
    -> RabiResult<nd::Array3<C64>>
{
    let dt: f64 = (t[1] - t[0]).abs();
    let rho0: nd::Array2<C64> = outer_prod(psi0, psi0);
    let mut rho: nd::Array3<C64>
        = nd::Array::zeros((psi0.len(), psi0.len(), t.len()));
    let mut drho: nd::Array2<C64>;
    let mut rho_new: nd::Array2<C64>;
    let mut N: C64;
    rho.slice_mut(s![.., .., 0]).assign(&rho0);
    rho.slice_mut(s![.., .., 1]).assign(&rho0);
    for k in 1..t.len() - 1 {
        drho = C64::from(2.0 * dt)
            * (
                -C64::i()
                    * commutator(
                        H.slice(s![.., .., k]),
                        rho.slice(s![.., .., k])
                    )
                + lindbladian(Y.view(), rho.slice(s![.., .., k]))
            );
        rho_new = &rho.slice(s![.., .., k - 1]) + drho;
        N = trace(&rho_new);
        (rho_new / N).move_into(rho.slice_mut(s![.., .., k + 1]));
    }
    return Ok(rho);
}

/// Numerically integrate the Lindblad equation using fourth-order Runge-Kutta
/// for a time-dependent Hamiltonian.
///
/// Requires `H` and `Y` to be in units of s^-1. The third index of `H`
/// corresponds to time.
pub fn lindblad_evolve_rk4(psi0: &nd::Array1<C64>, H: &nd::Array3<C64>,
                           Y: &nd::Array2<f64>, t: &nd::Array1<f64>)
    -> RabiResult<nd::Array3<C64>>
{
    let dt: f64 = (t[1] - t[0]).abs();
    let rho0: nd::Array2<C64> = outer_prod(psi0, psi0);
    let mut rho: nd::Array3<C64>
        = nd::Array::zeros((psi0.len(), psi0.len(), t.len()));
    let (mut r1, mut r2, mut r3, mut r4):
        (nd::Array2<C64>, nd::Array2<C64>, nd::Array2<C64>, nd::Array2<C64>);
    let mut rho_old: nd::ArrayView2<C64>;
    let mut rho_new: nd::Array2<C64>;
    let mut N: C64;
    rho.slice_mut(s![.., .., 0]).assign(&rho0);
    rho.slice_mut(s![.., .., 1]).assign(&rho0);
    let rhs = |H: nd::ArrayView2<C64>, p: nd::ArrayView2<C64>|
        -C64::i() * commutator(H, p) + lindbladian(Y.view(), p);
    for k in 0..t.len() - 2 {
        rho_old = rho.slice(s![.., .., k]);
        r1 = rhs(H.slice(s![.., .., k]), rho_old);
        r2 = rhs(
            H.slice(s![.., .., k + 1]),
            (&rho_old + r1.mapv(|r| dt * r)).view()
        );
        r3 = rhs(
            H.slice(s![.., .., k + 1]),
            (&rho_old + r2.mapv(|r| dt * r)).view()
        );
        r4 = rhs(
            H.slice(s![.., .., k + 2]),
            (&rho_old + r3.mapv(|r| 2.0 * dt * r)).view()
        );

        rho_new = &rho_old
            + (
                r1 + r2.mapv(|r| 2.0 * r) + r3.mapv(|r| 2.0 * r) + r4
            ).mapv(|r| dt / 3.0 * r);
        N = trace(&rho_new);

        (rho_new / N).move_into(rho.slice_mut(s![.., .., k + 2]));
    }
    return Ok(rho);
}

/// Find the index of the `M`-th local maximum in an array of oscillating
/// values.
pub fn find_2pi_idx(X: &nd::Array1<f64>, M: usize, eps: f64) -> (usize, f64) {
    let mut x0: f64 = X[0];
    let mut k0: usize = 0;
    let switch: Vec<i8>
        = (0..2 * M).map(|m| (1 - 2 * (m % 2)) as i8).collect();
    let mut m: usize = 0;
    for (k, x) in X.iter().enumerate() {
        if switch[m] as f64 * (x - x0) <= -eps {
            x0 = *x;
            k0 = k;
        } else if switch[m] as f64 * (x - x0) > eps {
            if m < 2 * M - 1 {
                m += 1;
            } else {
                break;
            }
        }
    }
    return (k0, x0);
}

