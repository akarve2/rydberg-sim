use std::{
    f64::consts::PI,
};
use ndarray as nd;
use num_complex::Complex64 as C64;
use crate::{
    zm_br_fn,
    mkstate,
    mkbasis,
    pulse_fn,
    pulse_fn_t,
    pulse_fn_poincare,
    pulse_fn_poincare_t,
    config_fn,
    error::*,
    hilbert::{
        *,
    },
    constants::{
        G_3P0_F12,
        G_1S0_F12,
        MU_B,
    },
};

pub const MU_C: f64 = MU_B * G_3P0_F12; // MHz G^-1
pub const MU_G: f64 = MU_B * G_1S0_F12; // MHz G^-1
zm_br_fn!(
    zm_br_g0 : {
        -1.10727922e-08,
         3.74955336e-04,
         7.63360649e-09,
         1.94851695e-02,
         1.39214067e-08,
    }
);
zm_br_fn!(
    zm_br_g1 : {
        -1.41632243e-06,
        -3.78301182e-04,
         1.41632838e-06,
         4.72462887e+00,
         5.58055706e+00,
    }
);
zm_br_fn!(
    zm_br_c0 : {
         1.01151916e+02,
         9.40966754e-05,
        -1.01151915e+02,
        -1.99260954e-06,
         2.02381457e-09,
    }
);
zm_br_fn!(
    zm_br_c1 : {
         4.64712454e+00,
        -2.98286729e-04,
        -4.64711085e+00,
        -4.43638992e-05,
         4.39216548e-08,
    }
);

pub fn zm(state: ClockState, B: f64) -> f64 {
    return match state {
        ClockState::g0 => zm_br_g0(B),
        ClockState::g1 => zm_br_g1(B),
        ClockState::c0 => zm_br_c0(B),
        ClockState::c1 => zm_br_c1(B),
    };
}

mkstate!(
    ClockState : {
        g0 = 0.5, -0.5,
        g1 = 0.5,  0.5,
        c0 = 0.5, -0.5,
        c1 = 0.5,  0.5,
    }
);

mkbasis!( ClockBasis[ClockState] );

pub enum PulseType {
    Sigma_p,
    Sigma_m,
    Pi,
}

pulse_fn!(
    H_sigma_p : {
        basis: ClockBasis : ClockState,
        principal:
            ClockState::g0 => ClockState::c1,
        parasitic: {
            ClockState::g0 => ClockState::c0,
            ClockState::g1 => ClockState::c1,
            ClockState::g1 => ClockState::c0,
        }
    }
);

pulse_fn!(
    H_sigma_m : {
        basis: ClockBasis : ClockState,
        principal:
            ClockState::g1 => ClockState::c0,
        parasitic: {
            ClockState::g1 => ClockState::c1,
            ClockState::g0 => ClockState::c1,
            ClockState::g0 => ClockState::c0,
        }
    }
);

pulse_fn!(
    H_pi : {
        basis: ClockBasis : ClockState,
        principal:
            ClockState::g0 => ClockState::c0,
        parasitic: {
            ClockState::g0 => ClockState::c1,
            ClockState::g1 => ClockState::c1,
            ClockState::g1 => ClockState::c0,
        }
    }
);

pub fn H_pulse(pulse_type: PulseType, basis: ClockBasis, w: f64, W: f64,
               chi: f64, t: nd::ArrayView1<f64>, phi: Option<f64>)
    -> HilbertResult<nd::Array3<C64>>
{
    return match pulse_type {
        PulseType::Sigma_p
            => H_sigma_p(basis, w, W, chi, t, phi.unwrap_or(0.0)),
        PulseType::Sigma_m
            => H_sigma_m(basis, w, W, chi, t, phi.unwrap_or(0.0)),
        PulseType::Pi
            => H_pi(basis, w, W, chi, t, phi.unwrap_or(0.0)),
    };
}

pulse_fn_poincare!(
    H_poincare : {
        basis: ClockBasis : ClockState,
        transitions: {
            ClockState::g1 => ClockState::c1,
            ClockState::g1 => ClockState::c0,
            ClockState::g0 => ClockState::c1,
            ClockState::g0 => ClockState::c0,
        },
        stretch: ClockState::g0 => ClockState::c1,
    }
);

pulse_fn_t!(
    H_sigma_p_t : {
        basis: ClockBasis : ClockState,
        principal:
            ClockState::g0 => ClockState::c1,
        parasitic: {
            ClockState::g0 => ClockState::c0,
            ClockState::g1 => ClockState::c1,
            ClockState::g1 => ClockState::c0,
        }
    }
);

pulse_fn_t!(
    H_sigma_m_t : {
        basis: ClockBasis : ClockState,
        principal:
            ClockState::g1 => ClockState::c0,
        parasitic: {
            ClockState::g1 => ClockState::c1,
            ClockState::g0 => ClockState::c1,
            ClockState::g0 => ClockState::c0,
        }
    }
);

pulse_fn_t!(
    H_pi_t : {
        basis: ClockBasis : ClockState,
        principal:
            ClockState::g0 => ClockState::c0,
        parasitic: {
            ClockState::g0 => ClockState::c1,
            ClockState::g1 => ClockState::c1,
            ClockState::g1 => ClockState::c0,
        }
    }
);

pub fn H_pulse_t(pulse_type: PulseType, basis: ClockBasis,
                 w: nd::ArrayView1<f64>, W: nd::ArrayView1<f64>,
                 chi: nd::ArrayView1<f64>, t: nd::ArrayView1<f64>,
                 phi: Option<f64>)
    -> HilbertResult<nd::Array3<C64>>
{
    return match pulse_type {
        PulseType::Sigma_p
            => H_sigma_p_t(basis, w, W, chi, t, phi.unwrap_or(0.0)),
        PulseType::Sigma_m
            => H_sigma_m_t(basis, w, W, chi, t, phi.unwrap_or(0.0)),
        PulseType::Pi
            => H_pi_t(basis, w, W, chi, t, phi.unwrap_or(0.0)),
    };
}

pulse_fn_poincare_t!(
    H_poincare_t : {
        basis: ClockBasis : ClockState,
        transitions: {
            ClockState::g1 => ClockState::c1,
            ClockState::g1 => ClockState::c0,
            ClockState::g0 => ClockState::c1,
            ClockState::g0 => ClockState::c0,
        },
        stretch: ClockState::g0 => ClockState::c1,
    }
);

/// Targeted keys
/// -------------
/// W = 0.1_f64
///     Drive strength in MHz. Returned in us^-1 (angular frequency).
/// ND = 100_usize
///     Number of points to use on each dimension of any renderable data array,
///     except for time.
/// Bmax = 200.0_f64
///     Upper end of the magnetic field strength plotting range in G.
config_fn!(
    "config.toml", "clock" => {
        "W", 5.0, f64
            => W : f64 = |W: f64| -> f64 { 2.0 * PI * W },
        "ND", 100, i64
            => ND : usize = |ND: i64| -> usize { ND as usize },
        "Bmax", 200.0, f64
            => Bmax : f64 = |B: f64| -> f64 { B },
    }
);

