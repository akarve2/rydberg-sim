use std::{
    f64::consts::PI,
};
use indexmap::IndexMap;
use itertools::Itertools;
use ndarray::{
    self as nd,
    s,
};
use num_complex::Complex64 as C64;
use toml;
use crate::{
    mkstate,
    mkbasis,
    mkprodbasis,
    pulse_fn,
    pulse_fn_t,
    pulse_fn_poincare,
    pulse_fn_poincare_t,
    config_fn,
    error::*,
    hilbert::{
        self,
        *,
    },
    constants::{
        G_3S1_F32,
        G_3P0_F12,
        MU_B,
    },
};

pub const MU_R: f64 = MU_B * G_3S1_F32; // MHz G^-1
pub const MU_C: f64 = MU_B * G_3P0_F12; // MHz G^-1
pub const C6: f64 = 300000.0; // MHz um^6
pub const R_SEP: f64 = 3.5; // um
pub const U0: f64
    = 2.0 * PI * C6 / (R_SEP * R_SEP * R_SEP * R_SEP * R_SEP * R_SEP); // us^-1

mkstate!(
    RydbergState : {
        c0  = 0.5, -0.5,
        c1  = 0.5,  0.5,
        r1m = 1.5, -1.5,
        r0m = 1.5, -0.5,
        r0p = 1.5,  0.5,
        r1p = 1.5,  1.5,
    }
);

impl RydbergState {
    fn is_rydberg(&self) -> bool {
        return match *self {
            RydbergState::c0 => false,
            RydbergState::c1 => false,
            RydbergState::r1m => true,
            RydbergState::r0m => true,
            RydbergState::r0p => true,
            RydbergState::r1p => true,
        };
    }
}

mkbasis!( RydbergBasis[RydbergState] );

impl RydbergBasis {
    pub fn to_multiatom(&self, N: usize) -> HilbertResult<RydbergProdBasis> {
        let mut energies: IndexMap<Vec<RydbergState>, f64> = IndexMap::new();
        let prodstate_idx
            = (0..N).map(|_| 0..self.len()).multi_cartesian_product();
        for kk in prodstate_idx {
            energies.insert(
                kk.iter().map(|k| *self.get_state(*k).unwrap()).collect(),
                kk.iter().map(|k| self.get_energy_i(*k).unwrap()).sum()
            );
        }
        return RydbergProdBasis::new(N, energies);
    }
}

mkprodbasis!( RydbergProdBasis[Vec<RydbergState>] );

pub enum PulseType {
    Sigma_p,
    Sigma_m,
    Pi,
}

pulse_fn!(
    H_sigma_p : {
        basis: RydbergBasis : RydbergState,
        principal:
            RydbergState::c1 => RydbergState::r1p,
        parasitic: {
            RydbergState::c1 => RydbergState::r0p,
            RydbergState::c1 => RydbergState::r0m,
            RydbergState::c0 => RydbergState::r0p,
            RydbergState::c0 => RydbergState::r0m,
            RydbergState::c0 => RydbergState::r1m,
        }
    }
);

pulse_fn!(
    H_sigma_m : {
        basis: RydbergBasis : RydbergState,
        principal:
            RydbergState::c1 => RydbergState::r0m,
        parasitic: {
            RydbergState::c1 => RydbergState::r1p,
            RydbergState::c1 => RydbergState::r0p,
            RydbergState::c0 => RydbergState::r0p,
            RydbergState::c0 => RydbergState::r0m,
            RydbergState::c0 => RydbergState::r1m,
        }
    }
);

pulse_fn!(
    H_pi : {
        basis: RydbergBasis : RydbergState,
        principal:
            RydbergState::c1 => RydbergState::r0p,
        parasitic: {
            RydbergState::c1 => RydbergState::r1p,
            RydbergState::c1 => RydbergState::r0m,
            RydbergState::c0 => RydbergState::r0p,
            RydbergState::c0 => RydbergState::r0m,
            RydbergState::c0 => RydbergState::r1m,
        }
    }
);

pub fn H_pulse(pulse_type: PulseType, basis: RydbergBasis, w: f64, W: f64,
               chi: f64, t: nd::ArrayView1<f64>, phi: Option<f64>)
    -> HilbertResult<nd::Array3<C64>>
{
    return match pulse_type {
        PulseType::Sigma_p
            => H_sigma_p(basis, w, W, chi, t, phi.unwrap_or(0.0)),
        PulseType::Sigma_m
            => H_sigma_m(basis, w, W, chi, t, phi.unwrap_or(0.0)),
        PulseType::Pi
            => H_pi(basis, w, W, chi, t, phi.unwrap_or(0.0)),
    };
}

pulse_fn_poincare!(
    H_poincare : {
        basis: RydbergBasis : RydbergState,
        transitions: {
            RydbergState::c1 => RydbergState::r1p,
            RydbergState::c1 => RydbergState::r0p,
            RydbergState::c1 => RydbergState::r0m,
            RydbergState::c0 => RydbergState::r0p,
            RydbergState::c0 => RydbergState::r0m,
            RydbergState::c0 => RydbergState::r1m,
        },
        stretch: RydbergState::c1 => RydbergState::r1p,
    }
);

pulse_fn_t!(
    H_sigma_p_t : {
        basis: RydbergBasis : RydbergState,
        principal:
            RydbergState::c1 => RydbergState::r1p,
        parasitic: {
            RydbergState::c1 => RydbergState::r0p,
            RydbergState::c1 => RydbergState::r0m,
            RydbergState::c0 => RydbergState::r0p,
            RydbergState::c0 => RydbergState::r0m,
            RydbergState::c0 => RydbergState::r1m,
        }
    }
);

pulse_fn_t!(
    H_sigma_m_t : {
        basis: RydbergBasis : RydbergState,
        principal:
            RydbergState::c1 => RydbergState::r0m,
        parasitic: {
            RydbergState::c1 => RydbergState::r1p,
            RydbergState::c1 => RydbergState::r0p,
            RydbergState::c0 => RydbergState::r0p,
            RydbergState::c0 => RydbergState::r0m,
            RydbergState::c0 => RydbergState::r1m,
        }
    }
);

pulse_fn_t!(
    H_pi_t : {
        basis: RydbergBasis : RydbergState,
        principal:
            RydbergState::c1 => RydbergState::r0p,
        parasitic: {
            RydbergState::c1 => RydbergState::r1p,
            RydbergState::c1 => RydbergState::r0m,
            RydbergState::c0 => RydbergState::r0p,
            RydbergState::c0 => RydbergState::r0m,
            RydbergState::c0 => RydbergState::r1m,
        }
    }
);

pub fn H_pulse_t(pulse_type: PulseType, basis: RydbergBasis,
                 w: nd::ArrayView1<f64>, W: nd::ArrayView1<f64>,
                 chi: nd::ArrayView1<f64>, t: nd::ArrayView1<f64>,
                 phi: Option<f64>)
    -> HilbertResult<nd::Array3<C64>>
{
    return match pulse_type {
        PulseType::Sigma_p
            => H_sigma_p_t(basis, w, W, chi, t, phi.unwrap_or(0.0)),
        PulseType::Sigma_m
            => H_sigma_m_t(basis, w, W, chi, t, phi.unwrap_or(0.0)),
        PulseType::Pi
            => H_pi_t(basis, w, W, chi, t, phi.unwrap_or(0.0)),
    };
}

pulse_fn_poincare_t!(
    H_poincare_t : {
        basis: RydbergBasis : RydbergState,
        transitions: {
            RydbergState::c1 => RydbergState::r1p,
            RydbergState::c1 => RydbergState::r0p,
            RydbergState::c1 => RydbergState::r0m,
            RydbergState::c0 => RydbergState::r0p,
            RydbergState::c0 => RydbergState::r0m,
            RydbergState::c0 => RydbergState::r1m,
        },
        stretch: RydbergState::c1 => RydbergState::r1p,
    }
);

#[derive(Copy, Clone, Debug, PartialEq)]
pub enum CouplingType { AllToAll, Chain }

fn multiatom_kron(N: usize, H: &nd::Array2<C64>)
    -> HilbertResult<nd::Array2<C64>>
{
    if !H.is_square() {
        return Err(HilbertError::NonSquareHamiltonian);
    }
    let n: usize = H.shape()[0];
    let mut eye1: nd::Array2<C64>;
    let mut eye2: nd::Array2<C64>;
    let mut term: nd::Array2<C64>;
    let mut acc: nd::Array2<C64>
        = nd::Array::zeros((n.pow(N as u32), n.pow(N as u32)));
    for k in 0..N {
        eye1 = nd::Array::eye(n.pow(k as u32));
        eye2 = nd::Array::eye(n.pow((N - k - 1) as u32));
        term
            = hilbert::kron(
                &hilbert::kron(
                    &eye1,
                    H
                ),
                &eye2
            ).into_dimensionality::<nd::Ix2>().unwrap();
        acc = &acc + &term;
    }
    return Ok(acc);
}

pub fn gen_multiatom(coupling_type: CouplingType, basis: RydbergProdBasis,
                     H: nd::ArrayView3<C64>, U: f64)
    -> HilbertResult<nd::Array3<C64>>
{
    let N_basis: usize = basis.len();
    let N_atoms: usize = basis.N;
    let N_time: usize = H.raw_dim()[2];
    let mut u: f64;
    let mut H_N: nd::Array3<C64> = nd::Array::zeros((N_basis, N_basis, N_time));
    for k in 0..N_time {
        H_N.slice_mut(s![.., .., k])
            .assign(
                &multiatom_kron(N_atoms, &H.slice(s![.., .., k]).to_owned())?);
        if coupling_type == CouplingType::AllToAll {
            for j in 0..N_basis {
                H_N[[j, j, k]]
                    += U * (
                        basis.get_state(j).unwrap()
                        .iter().map(|S| if S.is_rydberg() { 1.0 } else { 0.0 })
                        .sum::<f64>()
                        - 1.0
                    );
            }
        } else {
            for j in 0..N_basis {
                let S: &Vec<RydbergState> = basis.get_state(j).unwrap();
                u = S.iter().filter(|si1| si1.is_rydberg()).enumerate()
                    .map(|(i1, _)| {
                        S.iter().filter(|si2| si2.is_rydberg()).enumerate()
                        .map(|(i2, _)| U / (i1 as f64 - i2 as f64).powi(6))
                        .sum::<f64>()
                    })
                    .sum::<f64>();
                H_N[[j, j, k]] += C64::new(u, 0.0);
            }
        }
    }
    return Ok(H_N);
}

fn multiatom_kron_per(HH: Vec<nd::ArrayView2<C64>>)
    -> HilbertResult<nd::Array2<C64>>
{
    if HH.is_empty() {
        return Err(HilbertError::EmptyPer);
    }
    if !HH.iter().all(|H| H.is_square()) {
        return Err(HilbertError::NonSquareHamiltonian);
    }
    if !HH.iter().all(|H| H.raw_dim() == HH[0].raw_dim()) {
        return Err(HilbertError::NonEqSizeHamiltonian);
    }
    let n: usize = HH[0].shape()[0];
    let N: usize = HH.len();
    let mut eye1: nd::Array2<C64>;
    let mut eye2: nd::Array2<C64>;
    let mut term: nd::Array2<C64>;
    let mut acc: nd::Array2<C64> = nd::Array::zeros((N * n, N * n));
    for (k, H) in HH.iter().enumerate() {
        eye1 = nd::Array::eye(n.pow(k as u32));
        eye2 = nd::Array::eye(n.pow((N - k - 1) as u32));
        term
            = hilbert::kron(
                &hilbert::kron(
                    &eye1,
                    &H.to_owned()
                ),
                &eye2
            );
        acc = &acc + &term;
    }
    return Ok(acc);
}

pub fn gen_multiatom_per(coupling_type: CouplingType, basis: RydbergProdBasis,
                         HH: Vec<nd::ArrayView3<C64>>, U: f64)
    -> HilbertResult<nd::Array3<C64>>
{
    if HH.is_empty() {
        return Err(HilbertError::EmptyPer);
    }
    if !HH.iter().all(|H| H.raw_dim()[2] == HH[0].raw_dim()[2]) {
        return Err(HilbertError::NonEqTimeDep);
    }
    let N_basis: usize = basis.len();
    let N_time: usize = HH[0].raw_dim()[2];
    let mut u: f64;
    let mut H_N: nd::Array3<C64> = nd::Array::zeros((N_basis, N_basis, N_time));
    for k in 0..N_time {
        H_N.slice_mut(s![.., .., k])
            .assign(
                &multiatom_kron_per(
                    HH.iter().map(|H| H.slice(s![.., .., k])).collect()
                )?
            );
        if coupling_type == CouplingType::AllToAll {
            for j in 0..N_basis {
                H_N[[j, j, k]]
                    += U * (
                        basis.get_state(j).unwrap()
                        .iter().map(|S| if S.is_rydberg() { 1.0 } else { 0.0 })
                        .sum::<f64>()
                        - 1.0
                    );
            }
        } else {
            for j in 0..N_basis {
                let S: &Vec<RydbergState> = basis.get_state(j).unwrap();
                u = S.iter().filter(|si1| si1.is_rydberg()).enumerate()
                    .map(|(i1, _)| {
                        S.iter().filter(|si2| si2.is_rydberg()).enumerate()
                        .map(|(i2, _)| U / (i1 as f64 - i2 as f64).powi(6))
                        .sum::<f64>()
                    })
                    .sum::<f64>();
                H_N[[j, j, k]] += C64::new(u, 0.0);
            }
        }
    }
    return Ok(H_N);
}

/// Targeted keys
/// -------------
/// W = 6.0_f64
///     Drive strength in MHz. Returned in us^-1 (angular frequency).
/// r_sep = 3.5_f64
///     Inter-atom separation in um. Returns the equivalent Rydberg interaction
///     shift, U = C6 / r_sep^6 in us^-1.
/// ND = 100_usize
///     Number of points to use on each dimension of any renderable data array,
///     except for time.
/// Bmax = 150.0_f64
///     Upper end of the magnetic field strength plotting range in G.
config_fn!(
    "config.toml", "rydberg" => {
        "W", 6.0, f64
            => W : f64 = |W: f64| -> f64 { 2.0 * PI * W },
        "r_sep", 3.5, f64
            => U : f64 = |r: f64| -> f64 { 2.0 * PI * C6 / r.powi(6) },
        "ND", 100, i64
            => ND : usize = |ND: i64| -> usize { ND as usize },
        "Bmax", 150.0, f64
            => Bmax : f64 = |B: f64| -> f64 { B },
    }
);

