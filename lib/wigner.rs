use std::{
    ops::Range,
};
use crate::{
    clebsch_gordan::{
        self as cg,
        factorial,
        delta,
    },
};

fn sum_range_6j(j: f64, k1: f64, k2: f64, k3: f64, m1: f64, m2: f64, m3: f64)
    -> Range<i32>
{
    let start: i32 = j.max(k1.max(k2.max(k3))) as i32;
    let end: i32 = m1.max(m2.max(m3)) as i32 + 1;
    return start..end;
}

/// Evaluates the 3j symbol
/// [j1 j2 j3]
/// [m1 m2 m3]
pub fn wigner_3j(j1: f64, j2: f64, j3: f64, m1: f64, m2: f64, m3: f64) -> f64 {
    return
        (-1.0_f64).powi((j1 - j2 - m3) as i32)
        / (2.0 * j3 + 1.0).sqrt()
        * cg::cg(j1, m1, j2, m2, j3, -m3);
}

/// Evaluates the 6j symbol
/// {j1 j2 j3}
/// {l1 l2 l3}
///
/// The mapping to actual momenta is
/// {j1 j2 j12}
/// {j3 j  j23}
pub fn wigner_6j(j1: f64, j2: f64, j3: f64, l1: f64, l2: f64, l3: f64) -> f64 {
    let j: f64 = j1 + j2 + j3;
    let k1: f64 = j1 + l2 + l3;
    let k2: f64 = l1 + j2 + l3;
    let k3: f64 = l1 + l2 + j3;
    let m1: f64 = j1 + j2 + l1 + l2;
    let m2: f64 = j2 + j3 + l2 + l3;
    let m3: f64 = j3 + j1 + l3 + l1;

    let to_mul: f64
        = delta(j1, j2, j3)
        * delta(j1, l2, l3)
        * delta(l1, j2, l3)
        * delta(l1, l2, j3);

    let val: f64 = sum_range_6j(j, k1, k2, k3, m1, m2, m3)
        .map(|n| {
            (-1.0_f64).powi(n) * factorial(n + 1) as f64
            / (
                factorial(n - j as i32)
                * factorial(n - k1 as i32)
                * factorial(n - k2 as i32)
                * factorial(n - k3 as i32)
                * factorial(m1 as i32 - n)
                * factorial(m2 as i32 - n)
                * factorial(m3 as i32 - n)
            ) as f64
        }).sum();

    return to_mul * val;
}

/// Gives \braket{j_{12}, j_3, j}{j_1, j_{23}, j}
pub fn recoup3(j1: f64, j2: f64, j3: f64, j12: f64, j23: f64, j: f64) -> f64 {
    return
        (-1.0_f64).powi((j1 + j2 + j3 + j) as i32)
        * ((2.0 * j12 + 1.0) * (2.0 * j23 + 1.0)).sqrt()
        * wigner_6j(j1, j2, j12, j3, j, j23);
}

