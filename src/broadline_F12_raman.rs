#![allow(dead_code)]
#![allow(non_snake_case)]
#![allow(non_camel_case_types)]
#![allow(non_upper_case_globals)]
#![allow(clippy::needless_return)]

use std::{
    fs,
    path::PathBuf,
    f64::consts::PI,
};
use ndarray::{
    self as nd,
    s,
    array,
};
use num_complex::Complex64 as C64;
use sim::{
    loop_call,
    write_npz,
    rabi,
    systems::broadline_F12::{
        self as broadline,
        BroadlineState as GB,
        BroadlineBasis,
    },
    hilbert::{
        Basis,
    },
    utils,
};

fn do_raman(Delta: f64, ps: f64, ch: f64, B: f64, outpath: Option<PathBuf>)
    -> (f64, f64, f64)
{
    let config: broadline::Config = broadline::load_config(None);

    let t: nd::Array1<f64>
        = nd::Array::range(
            0.0,
            5.0,
            (2.0 * PI / config.W) / 1000.0
        );

    let basis: BroadlineBasis
        = BroadlineBasis::from_pairs([
            (GB::g0, 2.0 * PI * broadline::zm(GB::g0, B)),
            (GB::g1, 2.0 * PI * broadline::zm(GB::g1, B)),
            (GB::b0, 2.0 * PI * broadline::zm(GB::b0, B)),
            (GB::b1, 2.0 * PI * broadline::zm(GB::b1, B)),
        ]);

    let w: f64
        = basis.get_energy(&GB::b0).unwrap()
        - basis.get_energy(&GB::g0).unwrap()
        + (2.0 * PI * Delta);

    let H: nd::Array3<C64>
        = broadline::H_poincare(
            basis.clone(), w, config.W, ps, ch, PI / 2.0, t.view(), 0.0)
        .unwrap();
    let psi0: nd::Array1<C64> = basis.get_array(&GB::g0).unwrap();
    let phi: nd::Array1<C64> = basis.get_array(&GB::g1).unwrap();
    let bad: nd::Array1<C64> = basis.get_array(&GB::b0).unwrap();
    let psi: nd::Array2<C64> = rabi::schrodinger_evolve_rk4(&psi0, &H, &t)
        .unwrap();

    let P_psi0: nd::Array1<f64>
        = psi.t().dot(&psi0).mapv(|a| (a * a.conj()).re);
    let P_phi_c: nd::Array1<C64>
        = psi.t().dot(&phi).mapv(|a| a * a.conj());
    let P_phi: nd::Array1<f64>
        = P_phi_c.mapv(|aa| aa.re);
    let P_bad: nd::Array1<f64>
        = psi.t().dot(&bad).mapv(|a| (a * a.conj()).re);

    let (F, f): (nd::Array1<C64>, nd::Array1<f64>)
        = utils::do_fft(
            &(&P_phi_c - P_phi_c.mean().unwrap_or(C64::new(0.0, 0.0))),
            t[1] - t[0]
        );
    let FF: nd::Array1<f64> = F.mapv(|a| (a * a.conj()).re);

    let (k0, _): (usize, _) = rabi::find_2pi_idx(&P_psi0, 1, 0.9e-2);

    let W_eff_FT: f64 = f[utils::find_first_max(&FF).0];
    let T_2pi_FT = 1.0 / W_eff_FT;
    let T_2pi_FM = t[k0];
    let (T_2pi, W_eff): (f64, f64)
        = if (T_2pi_FT - T_2pi_FM).abs() / T_2pi_FT <= 0.05 {
            (T_2pi_FM, 1.0 / T_2pi_FM)
        } else {
            (T_2pi_FT, W_eff_FT)
        };

    if let Some(dir) = outpath {
        write_npz!(
            dir.join(format!(
                "monochr-cw\
                _W={:.1}MHz\
                _D={:.3}MHz\
                _psi={:.3}pi\
                _chi={:.3}pi\
                _B={:.1}G.npz",
                config.W / (2.0 * PI),
                Delta,
                ps / PI,
                ch / PI,
                B
            ).as_str()),
            arrays: {
                "t" => &t,
                "psi" => &psi,
                "g0" => &basis.get_array(&GB::g0).unwrap(),
                "g1" => &basis.get_array(&GB::g1).unwrap(),
                "b0" => &basis.get_array(&GB::b0).unwrap(),
                "b1" => &basis.get_array(&GB::b1).unwrap(),
                "f" => &f,
                "F" => &F,
                "FF" => &FF,
                "TW" => &array![T_2pi, W_eff],
            }
        );
    }

    return (
        *P_phi.slice(s![..k0 + 1]).iter()
            .max_by(|x, y| x.partial_cmp(y).unwrap()).unwrap(),
        *P_bad.slice(s![..k0 + 1]).iter()
            .max_by(|x, y| x.partial_cmp(y).unwrap()).unwrap(),
        W_eff,
    );
}

fn detuning(det_max: f64, B: Option<f64>, ND: Option<usize>, outpath: PathBuf,
        printflag: bool)
{
    if printflag { println!("detuning"); }
    let config: broadline::Config = broadline::load_config(None);

    let outfile: PathBuf = outpath.join("detuning.npz");
    if !outfile.is_file() {
        let D: nd::Array1<f64>
            = nd::Array::linspace(-det_max.abs(), 0.0, ND.unwrap_or(config.ND));
        let caller = |ijk: Vec<usize>| -> (f64, f64, f64) {
            do_raman(D[ijk[0]], PI / 4.0, 0.0, B.unwrap_or(config.B), None)
        };
        let (F, F_bad, W_eff):
            (nd::ArrayD<f64>, nd::ArrayD<f64>, nd::ArrayD<f64>)
            = loop_call!(
                caller => (F: f64, F_bad: f64, W_eff: f64),
                vars: { D }
            );
        write_npz!(
            outfile,
            arrays: {
                "D" => &D,
                "F" => &F,
                "F_bad" => &F_bad,
                "W_eff" => &W_eff,
            }
        );
    } else {
        println!("  Found existing data file; skipping computation...");
    }
}

fn polarization_angle(detuning: f64, B_max: Option<f64>, ND: Option<usize>,
    outpath: PathBuf, printflag: bool)
{
    if printflag { println!("polarization_angle"); }
    let config: broadline::Config = broadline::load_config(None);

    let outfile: PathBuf = outpath.join("polarization_angle.npz");
    if !outfile.is_file() {
        let psi: nd::Array1<f64>
            = nd::Array::linspace(
                PI / 12.0,
                5.0 * PI / 12.0,
                ND.unwrap_or(config.ND)
            );
        let B: nd::Array1<f64>
            = nd::Array::linspace(
                0.0,
                B_max.unwrap_or(config.B),
                ND.unwrap_or(config.ND)
            );
        let caller = |ijk: Vec<usize>| -> (f64, f64, f64) {
            do_raman(detuning, psi[ijk[0]], 0.0, B[ijk[1]], None)
        };
        let (F, F_bad, W_eff):
            (nd::ArrayD<f64>, nd::ArrayD<f64>, nd::ArrayD<f64>)
            = loop_call!(
                caller => (F: f64, F_bad: f64, W_eff: f64),
                vars: { psi, B }
            );
        write_npz!(
            outfile,
            arrays: {
                "psi" => &psi,
                "B" => &B,
                "F" => &F,
                "F_bad" => &F_bad,
                "W_eff" => &W_eff,
            }
        );
    } else {
        println!("  Found existing data file; skipping computation...");
    }
}

fn main() {
    let outpath = PathBuf::from("output/broadline_F12_raman");
    if !outpath.is_dir() {
        fs::create_dir_all(outpath.as_path())
            .unwrap_or_else(|_| {
                panic!(
                    "Couldn't create directory {:?}",
                    outpath.to_str().unwrap()
                )
            });
        println!(":: mkdir -p {:?}", outpath.to_str().unwrap());
    }

    let config: broadline::Config = broadline::load_config(None);

    println!(
        "broadline raman (F = 1/2)\n\
        =========================\n\
        Omega = {} MHz\n\
        Omega_eff = {} MHz\n\
        ND = {}\n\
        =========================",
        config.W / (2.0 * PI),
        config.Weff / (2.0 * PI),
        config.ND
    );

    println!("single-run tests");
    let tests: Vec<(f64, f64, f64)>
        = vec![ // (Delta, psi, B)
            (  -0.0_f64, PI / 12.0, 500.0_f64),
            ( -50.0_f64, PI / 12.0, 500.0_f64),
            (-100.0_f64, PI / 12.0, 500.0_f64),
            (-200.0_f64, PI / 12.0, 500.0_f64),
            (-300.0_f64, PI / 12.0, 500.0_f64),
        ];
    let caller = |ijk: Vec<usize>| -> (f64, f64, f64) {
        do_raman(tests[ijk[0]].0, tests[ijk[0]].1, 0.0, tests[ijk[0]].2,
            Some(outpath.clone()))
    };
    loop_call!(
        caller => (F: f64, F_bad: f64, W_eff: f64),
        vars: { tests }
    );

    let Dmax: f64 = config.W.powi(2) / (2.0 * config.Weff) / (2.0 * PI);

    detuning(Dmax + 100.0, Some(200.0), None, outpath.clone(), true);

    polarization_angle(125.0, Some(500.0), None, outpath.clone(), true);
}

