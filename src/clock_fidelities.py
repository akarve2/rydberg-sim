import numpy as np
import lib.rabi as rabi
import lib.systems.clock as clock
from lib.systems.clock import PulseType
import lib.plotdefs as pd
import sys
import pathlib

pd.set_font("myriad-ttf/MyriadPro-Regular.ttf", "MyriadPro")

ext = ".pdf"
global ND
ND = 100 # 2D plot grid density for both chi and B

outpath = pathlib.Path("output/clock_single-pulse_fidelities")
if not outpath.is_dir():
    print(f":: mkdir -p {outpath}")
    outpath.mkdir(parents=True)

B_array = np.loadtxt('atomic_data/b_field_1000G.txt')
zm_shifts = np.array([
    np.loadtxt('atomic_data/zm_br_ground_m1h.txt'),
    np.loadtxt('atomic_data/zm_br_ground_p1h.txt'),
    np.loadtxt('atomic_data/zm_br_clock_m1h.txt'),
    np.loadtxt('atomic_data/zm_br_clock_p1h.txt')
])

def get_zm_shift(B_target, state):
    index = np.argmin(np.abs(B_array - B_target))
    return zm_shifts[state,index]

#global basis
#basis = rydberg.BASIS
#basis = rydberg.Basis(["g1", "r0+", "r1+"])

def get_2pi_idx(P: np.ndarray):
    """
    Find the index that corresponds to the (effective) 2*pi pulse time from the
    probability time series `P` by finding the first local maximum after the
    first local minimum.
    """
    EPSILON = 1e-3
    P0 = P[0]
    k0 = 0
    dswitch = +1
    for k, p in enumerate(P):
        if dswitch * (p - P0) <= -EPSILON:
            P0 = p
            k0 = k
        elif dswitch * (p - P0) > EPSILON:
            if dswitch > 0:
                dswitch = -dswitch
            else:
                break
    return k0

global B0, mu_g, mu_r, noise, W0, d0, D0, chi0, dw, U0, t, nn
B0 = 250 # G
nn = 0 # std of shot-to-shot B-field noise; G
NN = 1 # trials to average over for B-field noise
mu_g = 1.0e-3 # MHz / G
mu_r = 1.4 # MHz / G
noise = 1e-5
W0 = 2 * np.pi * 100e-3 # (us)^-1
d0 = 2 * np.pi * mu_g * B0 # (us)^-1
D0 = 2 * np.pi * mu_r * B0 # (us)^-1
chi0 = 0.1
U0 = -2 * np.pi * 60 # (us)^-1
t = np.arange(
    0.000,
    20.000,
    2 * np.pi / W0 / 1500
) # us
global value_str
value_str = f"B={B0:.1f}G_W={W0 / (2 * np.pi):.1f}MHz"
print(
f"""
=====
B = {B0:g} G
Omega = 2pi * {W0 / (2 * np.pi):g} MHz
delta = 2pi * {mu_g * B0:g} MHz
Delta = 2pi * {mu_r * B0:g} MHz
U = 2pi * {U0 / (2 * np.pi):g} MHz
chi = {chi0:g}
=====
"""[1:-1]
)

CMAP = pd.colormaps["fire-ice"]

def _single_atom_sigma_p(B=B0, chi=chi0, printflag=True, plotflag=True,
        cropflag=False):
    global B0, mu_g, mu_r, noise, W0, d0, D0, chi0, U0, t, nn
    global value_str
    # t_ = t[np.where(t <= 1.25 * np.pi / W0)] if cropflag else t
    t_ = t
    b = clock.Basis({
        "g0": 2 * np.pi * get_zm_shift(B, 0),
        "g1": 2 * np.pi * get_zm_shift(B, 1),
        "c0": 2 * np.pi * get_zm_shift(B, 2),
        "c1": 2 * np.pi * get_zm_shift(B, 3),
    })
    w = b.energy("c1") - b.energy("g0")
    W = W0 * np.ones(t_.shape)
    psi0 = b["g0"]

    H = clock.H_pulse(clock.PulseType.Sigma_p, b, w, W, chi, t_)
    # H = clock.H_pulse(clock.PulseType.Sigma_p_var, b, 0, W, chi, t_)
    psi = rabi.schrodinger_evolve_rk4(psi0, H, t_)

    phi = b["c1"]
    P_phi = np.abs(psi.T @ phi.conj())**2
    k0 = get_2pi_idx(np.abs(psi.T @ psi0.conj())**2)
    F = P_phi[:k0 + 1].max()
    
    if plotflag:
        measure = [
            ("g_0", b["g0"], "C0"),
            ("c_1", b["c1"], "C1"),
        ]
        P = pd.Plotter()
        for l, s, c in measure:
            P.plot(
                t_,
                np.abs(psi.T @ s.conj())**2,
                label=f"$\\left| {l} \\right\\rangle$",
                color=c
            )
        (P
            .axvline(t[k0], color="0.65", linestyle="--")
            .legend(loc="center left", fontsize="xx-small")
            .ggrid()
            .set_xlabel("Time [μs]", fontsize="x-small")
            .set_ylabel("Probability", fontsize="x-small")
            .tick_params(labelsize="x-small")
            .savefig(
                outpath.joinpath(f"single_W={W0 / (2 * np.pi):.1f}_B={B:.1f}G_chi={chi:.2f}{ext}"))
            .close()
        )
        X = np.vstack((
            t_,
            np.array([np.abs(psi.T @ s.conj())**2 for l, s, c in measure])
        )).T
        np.savetxt(
            outpath.joinpath(f"single_W={W0 / (2 * np.pi):.1f}_B={B:.1f}G_chi={chi:.2f}.txt"),
            X
        )

    return F
    return F if P_bad < 0.01 else np.float64("nan")

def single_atom_sigma_p(printflag=True):
    if printflag: print("single_atom_sigma_p")
    B = np.linspace(0, 100, 50)
    F = np.array([
        _single_atom_sigma_p(b, printflag=False, plotflag=False, cropflag=True)
        for b in B
    ])

    Bplot = np.linspace(B.min(), B.max(), 1000)
    (pd.Plotter()
        .plot(B, F, "oC0")
        .axhline(F[-1], color="C0")
        .text(B.min(), F[-1], f"{F[-1]:g}", fontsize="xx-small",
            horizontalalignment="left", verticalalignment="bottom")
        #.plot(Bplot, 1 - W0 / (chi0 * mu_r * Bplot), "--k")
        .set_ylim(-0.05, 1.05)
        .set_xlabel("Field strength [G]")
        .set_ylabel("Fidelity [$P_{\\left| c_1 \\right\\rangle}$]")
        .ggrid()
        .savefig(
            outpath.joinpath(f"single-atom_W={W0 / (2 * np.pi):.1f}MHz{ext}"))
        .close()
    )

def single_atom_sigma_p_2d(printflag=True):
    if printflag: print("single_atom_sigma_p_2d")

    B_dump \
        = outpath.joinpath(f"single-atom-B_W={W0 / (2 * np.pi):.1f}MHz.txt")
    chi_dump \
        = outpath.joinpath(f"single-atom-chi_W={W0 / (2 * np.pi):.1f}MHz.txt")
    data_dump \
        = outpath.joinpath(f"single-atom-2d_W={W0 / (2 * np.pi):.1f}MHz.txt")
    if data_dump.exists() and B_dump.exists() and chi_dump.exists():
        print("  Found data dump files")
        B = np.loadtxt(B_dump)
        dB = B[1] - B[0]
        chi = np.loadtxt(chi_dump)
        logchi = np.log10(chi)
        dchi = logchi[1] - logchi[0]
        F = np.loadtxt(data_dump)
    else:
        B = B_array[::len(B_array) // ND]
        dB = B[1] - B[0]
        chi = np.logspace(-2, np.log10(2 / 3), ND)
        logchi = np.log10(chi)
        dchi = logchi[1] - logchi[0]
        F = np.zeros((chi.shape[0], B.shape[0]), dtype=np.float64)
        for i, x in enumerate(chi):
            for j, b in enumerate(B):
                print("\r{:4.0f} / {:4.0f}; {:4.0f} / {:4.0f}  ({:5.1f}%) "
                    .format(
                        i + 1, chi.shape[0],
                        j + 1, B.shape[0],
                        100 * (i * B.shape[0] + j + 1) / (chi.shape[0] * B.shape[0])
                    ),
                    end="", flush=True
                )
                F[i, j] = _single_atom_sigma_p(
                        B=b, chi=x, printflag=False, plotflag=False, cropflag=True)
        print("")
        np.savetxt(B_dump, B)
        np.savetxt(chi_dump, chi)
        np.savetxt(data_dump, F)

    BB, XX = np.meshgrid(B, logchi)
    (pd.Plotter()
        .imshow(np.log10(1 - F),
            extent=[
                B.min() - dB / 2, B.max() + dB / 2,
                logchi.min() - dchi / 2, logchi.max() + dchi / 2
            ],
            origin="lower", aspect="auto", cmap=CMAP)
        .colorbar()
        .contour(BB, XX, np.log10(1 - F), levels=[np.log10(1 - 0.99)],
            colors="k", linestyles="-")
        .set_xlabel("Field strength [G]")
        .set_ylabel("$\\log_{10} \\chi$")
        .set_clabel("$\\log_{10} (1 - P_{\\left| c_1 \\right\\rangle})$")
        #.set_clim(F.min(), 1)
        .grid(False)
        .savefig(
            outpath.joinpath(f"single-atom-2d_W={W0 / (2 * np.pi):.1f}MHz{ext}"))
        .close()
    )

    _single_atom_sigma_p(
        100, 0.01, printflag=False, plotflag=True, cropflag=False)
    _single_atom_sigma_p(
        100, 0.67, printflag=False, plotflag=True, cropflag=False)
    _single_atom_sigma_p(
        500, 0.01, printflag=False, plotflag=True, cropflag=False)
    _single_atom_sigma_p(
        500, 0.67, printflag=False, plotflag=True, cropflag=False)

def _double_atom_sigma_p(B=B0, chi=chi0, printflag=True, plotflag=True,
        cropflag=False):
    global basis
    global B0, mu_g, mu_r, noise, W0, d0, D0, chi0, U0, t, nn
    global value_str
    t_ = t[np.where(t <= 1.25 * np.pi / W0)] if cropflag else t
    chi_ = chi * np.ones(t_.shape)
    d = (2 * np.pi * mu_g * B) * np.ones(t_.shape)
    D = (2 * np.pi * mu_r * B) * np.ones(t_.shape)
    w = 1.5 * D + 0.5 * d
    W = W0 * np.ones(t_.shape)
    U = U0 * np.ones(t_.shape)

    H = rydberg.H_pulse(PulseType.Sigma_p, basis, w, d, D, W, chi_, t_)
    b, H = rydberg.gen_multiatom(basis, 2, H, U)
    psi0 = b["g1,g1"]
    psi = rabi.schrodinger_evolve_rk4(psi0, H, t_)

    if plotflag:
        measure = [
            ("g_1 g_1", b["g1,g1"], "C0"),
            ("B", (b["g1,r1+"] + b["r1+,g1"]) / np.sqrt(2), "C1"),
            ("D", (b["g1,r1+"] - b["r1+,g1"]) / np.sqrt(2), "C2"),
        ]
        P = pd.Plotter()
        for l, s, c in measure:
            P.plot(
                t_,
                np.abs(psi.T @ s.conj())**2,
                label=f"$\\left| {l} \\right\\rangle$",
                color=c
            )
        (P
            .legend(loc="center left", fontsize="xx-small")
            .ggrid()
            .set_xlabel("Time [μs]", fontsize="x-small")
            .set_ylabel("Probability", fontsize="x-small")
            .tick_params(labelsize="x-small")
            .savefig(
                outpath.joinpath(f"double_W={W0 / (2 * np.pi):.1f}_U={U0 / (2 * np.pi):.1f}_B={B:.1f}G_chi={chi:.2f}{ext}"))
            .close()
        )
        X = np.vstack((
            t_,
            np.array([np.abs(psi.T @ s.conj())**2 for l, s, c in measure])
        )).T
        np.savetxt(
            outpath.joinpath(f"double_W={W0 / (2 * np.pi):.1f}_U={U0 / (2 * np.pi):.1f}_B={B:.1f}G_chi={chi:.2f}.txt"),
            X
        )

    phi = (b["g1,r1+"] + b["r1+,g1"]) / np.sqrt(2)
    F = (np.abs(psi.T @ phi.conj())**2).max()
    return F

def double_atom_sigma_p(printflag=True):
    if printflag: print("double_atom_sigma_p")
    B = np.linspace(50, 150, 50)
    F = np.array([
        _double_atom_sigma_p(b, printflag=False, plotflag=False, cropflag=True)
        for b in B
    ])

    Bfit = np.linspace(B.min(), B.max(), 1000)
    (pd.Plotter()
        .plot(B, F, "oC1")
        .axhline(F[-1], color="C1")
        .text(B.min(), F[-1], f"{F[-1]:g}", fontsize="xx-small",
            horizontalalignment="left", verticalalignment="bottom")
        #.plot(Bfit, 1 - 1 / (c[0] * Bfit), "--k")
        .set_xlabel("Field strength [G]")
        .set_ylabel("Fidelity [$P_{\\left| B \\right\\rangle}$]")
        .ggrid()
        .savefig(
            outpath.joinpath(f"double-atom_W={W0 / (2 * np.pi):.1f}MHz_U={U0 / (2 * np.pi):.1f}MHz{ext}"))
        .close()
    )

def double_atom_sigma_p_2d(printflag=True):
    if printflag: print("double_atom_sigma_p_2d")

    B_dump \
        = outpath.joinpath(f"double-atom-B_W={W0 / (2 * np.pi):.1f}MHz_U={U0 / (2 * np.pi):.1f}MHz.txt")
    chi_dump \
        = outpath.joinpath(f"double-atom-chi_W={W0 / (2 * np.pi):.1f}MHz_U={U0 / (2 * np.pi):.1f}MHz.txt")
    data_dump \
        = outpath.joinpath(f"double-atom-2d_W={W0 / (2 * np.pi):.1f}MHz_U={U0 / (2 * np.pi):.1f}MHz.txt")
    if data_dump.exists() and B_dump.exists() and chi_dump.exists():
        print("  Found data dump files")
        B = np.loadtxt(B_dump)
        dB = B[1] - B[0]
        chi = np.loadtxt(chi_dump)
        logchi = np.log10(chi)
        dchi = logchi[1] - logchi[0]
        F = np.loadtxt(data_dump)
    else:
        B = np.linspace(0, 200, ND)
        dB = B[1] - B[0]
        chi = np.logspace(-2, np.log10(2 / 3), ND)
        logchi = np.log10(chi)
        dchi = logchi[1] - logchi[0]
        F = np.zeros((chi.shape[0], B.shape[0]), dtype=np.float64)
        for i, e in enumerate(chi):
            for j, b in enumerate(B):
                print("\r{:4.0f} / {:4.0f}; {:4.0f} / {:4.0f}  ({:5.1f}%) "
                    .format(
                        i + 1, chi.shape[0],
                        j + 1, B.shape[0],
                        100 * (i * ND + j + 1) / ND**2
                    ),
                    end="", flush=True
                )
                F[i, j] = _double_atom_sigma_p(
                        B=b, chi=e, printflag=False, plotflag=False, cropflag=True)
        print("")
        np.savetxt(B_dump, B)
        np.savetxt(chi_dump, chi)
        np.savetxt(data_dump, F)

    (pd.Plotter()
        .imshow(np.log10(1 - F),
            extent=[
                B.min() - dB / 2, B.max() + dB / 2,
                logchi.min() - dchi / 2, logchi.max() + dchi / 2
            ],
            origin="lower", aspect="auto", cmap=CMAP)
        .colorbar()
        .set_xlabel("Field strength [G]")
        .set_ylabel("$\\log_{10} \\chi$")
        .set_clabel("$\\log_{10} (1 - P_{\\left| B \\right\\rangle})$")
        .grid(False)
        .savefig(
            outpath.joinpath(f"double-atom-2d_W={W0 / (2 * np.pi):.1f}MHz_U={U0 / (2 * np.pi):.1f}MHz{ext}"))
        .close()
    )

    _double_atom_sigma_p(
        B.min(), chi.min(), printflag=False, plotflag=True, cropflag=False)
    _double_atom_sigma_p(
        B.min(), chi.max(), printflag=False, plotflag=True, cropflag=False)
    _double_atom_sigma_p(
        B.max(), chi.min(), printflag=False, plotflag=True, cropflag=False)
    _double_atom_sigma_p(
        B.max(), chi.max(), printflag=False, plotflag=True, cropflag=False)

if __name__ == "__main__":
    #single_atom_sigma_p()
    #double_atom_sigma_p()
    single_atom_sigma_p_2d()
    #double_atom_sigma_p_2d()

