import numpy as np
import numpy.linalg as la
import lib.rabi as rabi
import lib.systems.clock as clock
from lib.systems.clock import PulseType
import lib.plotdefs as pd
import sys
import pathlib

pd.set_font("myriad-ttf/MyriadPro-Regular.ttf", "MyriadPro")

ext = ".pdf"
ND = 100 # 2D plot grid density for both chi and B

outpath = pathlib.Path("output/clock_single-pulse_phases")
if not outpath.is_dir():
    print(f":: mkdir -p {outpath}")
    outpath.mkdir(parents=True)

B_array = np.loadtxt('atomic_data/b_field_1000G.txt')
zm_shifts = np.array([
    np.loadtxt('atomic_data/zm_br_ground_m1h.txt'),
    np.loadtxt('atomic_data/zm_br_ground_p1h.txt'),
    np.loadtxt('atomic_data/zm_br_clock_m1h.txt'),
    np.loadtxt('atomic_data/zm_br_clock_p1h.txt')
])

def get_zm_shift(B_target, state):
    index = np.argmin(np.abs(B_array - B_target))
    return zm_shifts[state,index]

#global basis
#basis = rydberg.BASIS
#basis = rydberg.Basis(["g1", "r0+", "r1+"])

global B0, mu_g, mu_r, W0, d0, D0, chi0, U0, t, nn
B0 = 250 # G
nn = 0 # std of shot-to-shot B-field noise; G
NN = 1 # trials to average over for B-field noise
mu_g = 1.0e-3 # MHz / G
mu_r = 1.4 # MHz / G
W0 = 2 * np.pi * 100e-3 # (us)^-1
d0 = 2 * np.pi * mu_g * B0 # (us)^-1
D0 = 2 * np.pi * mu_r * B0 # (us)^-1
chi0 = 0.1
U0 = -2 * np.pi * 60 # (us)^-1
t0 = 0.0
t = np.arange(
    0.000,
    20.000,
    2 * np.pi / W0 / 1500
) # us
global value_str
value_str = f"B={B0:.1f}G_W={W0 / (2 * np.pi):.1f}MHz"
print(
f"""
=====
B = {B0:g} G
Omega = 2pi * {W0 / (2 * np.pi):g} MHz
delta = 2pi * {mu_g * B0:g} MHz
Delta = 2pi * {mu_r * B0:g} MHz
U = 2pi * {U0 / (2 * np.pi):g} MHz
chi = {chi0:g}
=====
"""[1:-1]
)

CMAP = pd.colormaps["fire-ice"]

THETA0 = np.pi / 2
PHI0 = 0

def phase_angle(z: np.complex128) -> np.float64:
    return np.log(z / abs(z)).imag

def get_2pi_idx(P: np.ndarray, m=1):
    """
    Find the index that corresponds to the (effective) 2*pi pulse time from the
    probability time series `P` by finding the `m`-th local maximum after the
    `m`-th local minimum.
    """
    EPSILON = 1e-3
    P0 = P[0]
    k0 = 0
    dswitch = m * [+1, -1]
    _m = 0
    for k, p in enumerate(P):
        if dswitch[_m] * (p - P0) <= -EPSILON:
            P0 = p
            k0 = k
        elif dswitch[_m] * (p - P0) > EPSILON:
            if _m < 2 * m - 1:
                _m += 1
            else:
                break
    return k0

def plot_bloch_sphere(a: np.complex128, b: np.complex128, k0: int,
        labels: list[str]) -> pd.Plotter:
    th = np.linspace(0, np.pi, 50)
    ph = np.linspace(0, 2 * np.pi, 50)
    TH, PH = np.meshgrid(th, ph)
    X = np.sin(TH) * np.cos(PH)
    Y = np.sin(TH) * np.sin(PH)
    Z = np.cos(TH)

    P = (pd.Plotter.new_3d(figsize=(2.25, 1.8))
        .plot_surface(X, Y, Z, alpha=0.25, color="#f0f0f0")
        .plot(np.cos(ph), np.sin(ph), np.zeros(ph.shape),
            color="#e8e8e8", linewidth=0.5)
        .plot(np.sin(ph), np.zeros(ph.shape), np.cos(ph),
            color="#e8e8e8", linewidth=0.5)
        .plot(np.zeros(ph.shape), np.sin(ph), np.cos(ph),
            color="#e8e8e8", linewidth=0.5)
        .plot([-1, 0], [0, 0], [0, 0], color="#e8e8e8", linestyle=":",
            linewidth=0.5)
        .plot([0, +1], [0, 0], [0, 0], color="#e8e8e8", linestyle="--",
            linewidth=0.5)
        .plot([0, 0], [-1, 0], [0, 0], color="#e8e8e8", linestyle=":",
            linewidth=0.5)
        .plot([0, 0], [0, +1], [0, 0], color="#e8e8e8", linestyle="--",
            linewidth=0.5)
        .plot([0, 0], [0, 0], [-1, 0], color="#e8e8e8", linestyle=":",
            linewidth=0.5)
        .plot([0, 0], [0, 0], [0, +1], color="#e8e8e8", linestyle="--",
            linewidth=0.5)
        .scatter([0], [0], [0], color="k", s=1)
        #.scatter([1], [0], [0], color="#e0e0e0", s=0.5)
        #.scatter([0], [1], [0], color="#e0e0e0", s=0.5)
        #.scatter([0], [0], [1], color="#e0e0e0", s=0.5)
        .text(0, 0, +1.1, labels[0],
            horizontalalignment="center",
            verticalalignment="bottom",
            fontsize="x-small"
        )
        .text(0, 0, -1.1, labels[1],
            horizontalalignment="center",
            verticalalignment="top",
            fontsize="x-small"
        )
        .set_box_aspect((1, 1, 1))
        .axis("off")
    )

    N = np.sqrt(abs(a)**2 + abs(b)**2)
    a_ = a / N
    b_ = b / N
    th_ = 2 * np.arccos(abs(a_))
    ph_ = np.arctan2(b_.imag, b_.real) - np.arctan2(a_.imag, a_.real)
    (P
        .plot(
            1.01 * np.sin(th_) * np.cos(ph_),
            1.01 * np.sin(th_) * np.sin(ph_),
            1.01 * np.cos(th_)
        )
        .scatter(
            1.01 * np.sin(th_[0]) * np.cos(ph_[0]),
            1.01 * np.sin(th_[0]) * np.sin(ph_[0]),
            1.01 * np.cos(th_[0]),
            color="g", s=0.5
        )
        .scatter(
            1.01 * np.sin(th_[k0]) * np.cos(ph_[k0]),
            1.01 * np.sin(th_[k0]) * np.sin(ph_[k0]),
            1.01 * np.cos(th_[k0]),
            color="0.65", s=0.5
        )
        .scatter(
            1.01 * np.sin(th_[-1]) * np.cos(ph_[-1]),
            1.01 * np.sin(th_[-1]) * np.sin(ph_[-1]),
            1.01 * np.cos(th_[-1]),
            color="r", s=0.5
        )
        .view_init(30, 30)
    )
    return P

def _single_atom_sigma_p(B=B0, chi=chi0, printflag=True, plotflag=True):
    global basis
    global B0, mu_g, mu_r, W0, d0, D0, chi0, U0, t, nn
    global value_str
    b = clock.Basis({
        "g0": 2 * np.pi * get_zm_shift(B, 0),
        "g1": 2 * np.pi * get_zm_shift(B, 1),
        "c0": 2 * np.pi * get_zm_shift(B, 2),
        "c1": 2 * np.pi * get_zm_shift(B, 3),
    })
    w = b.energy("c1") - b.energy("g0")
    W = W0 * np.ones(t.shape)

    psi0 = (
        np.cos(THETA0 / 2) * b["g0"]
        + np.exp(1j * PHI0) * np.sin(THETA0 / 2) * b["g1"]
    )

    H = clock.H_pulse(PulseType.Sigma_p, b, w, W, chi, t)
    # H = clock.H_pulse(PulseType.Sigma_p_var, b, 0, W, chi, t)
    psi = rabi.schrodinger_evolve_rk4(psi0, H, t)

    g0 = b["g0"]
    g1 = b["g1"]
    c1 = b["c1"]

    k0 = get_2pi_idx(np.abs(psi.T @ g0.conj())**2)
    ph = phase_angle((psi[:, k0] @ g1.conj()) / (psi[:, k0] @ g0.conj()))

    if plotflag:
        measure = [
            ("g_0", b["g0"], "C0"),
            ("g_1", b["g1"], "C1"),
            ("c_1", b["c1"], "C3"),
        ]
        P = pd.Plotter.new(nrows=2, sharex=True)
        for l, s, c in measure:
            P[0].plot(
                t,
                np.abs(psi.T @ s.conj())**2,
                label=f"$\\left| {l} \\right\\rangle$",
                color=c
            )
            P[1].plot(
                t,
                phase_angle(psi.T @ s.conj()) / np.pi,
                color=c
            )
        (P[1]
            .plot(
                t,
                phase_angle((psi.T @ g1.conj()) / (psi.T @ g0.conj())) / np.pi,
                color="k",
                label="$g_1 - g_0$"
            )
            .plot(
                t,
                phase_angle((psi.T @ c1.conj()) / (psi.T @ g0.conj())) / np.pi,
                color="0.5",
                label="$c_1 - g_0$"
            )
        )

        (P[0]
            .axvline(t[k0], color="0.65", linestyle="--")
            .legend(loc="upper left", fontsize="xx-small")
            .ggrid()
            .set_ylabel("Probability")
        )
        (P[1]
            .axvline(t[k0], color="0.65", linestyle="--")
            .legend(loc="upper left", fontsize="xx-small")
            .ggrid()
            .set_ylabel("$\\Delta \\varphi / \\pi$")
            .set_xlabel("Time [μs]")
        )
        (P
            .tight_layout(h_pad=0.1)
            .savefig(
                outpath.joinpath(f"single_W={W0 / (2 * np.pi):.1f}_B={B:.1f}G_chi={chi:.2f}{ext}"))
            .close()
        )

        X = np.vstack((
            t,
            np.array([np.abs(psi.T @ s.conj())**2 for l, s, c in measure])
        )).T
        np.savetxt(
            outpath.joinpath(f"single_W={W0 / (2 * np.pi):.1f}_B={B:.1f}G_chi={chi:.2f}.txt"),
            X
        )

        (plot_bloch_sphere(
                psi.T @ g0.conj(), psi.T @ g1.conj(), k0, ["$g_0$", "$g_1$"])
            .savefig(
                outpath.joinpath(f"single-bloch_g0_g1_W={W0 / (2 * np.pi):.1f}_B={B:.1f}G_chi={chi:.2f}{ext}"))
            .close()
        )

        (plot_bloch_sphere(
                psi.T @ g0.conj(), psi.T @ b["c1"].conj(), k0, ["$g_0$", "$c_1$"])
            .savefig(
                outpath.joinpath(f"single-bloch_g0_c1_W={W0 / (2 * np.pi):.1f}_B={B:.1f}G_chi={chi:.2f}{ext}"))
            .close()
        )

    return ph
    return ph if P_bad < 0.01 else np.float64("nan")

def single_atom_sigma_p_2d(printflag=True):
    if printflag: print("single_atom_sigma_p_2d")

    B_dump \
        = outpath.joinpath(f"single-atom-B_W={W0 / (2 * np.pi):.1f}MHz.txt")
    chi_dump \
        = outpath.joinpath(f"single-atom-chi_W={W0 / (2 * np.pi):.1f}MHz.txt")
    data_dump \
        = outpath.joinpath(f"single-atom-2d_W={W0 / (2 * np.pi):.1f}MHz.txt")
    if data_dump.exists() and B_dump.exists() and chi_dump.exists():
        print("  Found data dump files")
        B = np.loadtxt(B_dump)
        dB = B[1] - B[0]
        chi = np.loadtxt(chi_dump)
        logchi = np.log10(chi)
        dchi = logchi[1] - logchi[0]
        PH = np.loadtxt(data_dump)
    else:
        B = B_array[::len(B_array) // ND]
        dB = B[1] - B[0]
        chi = np.logspace(-2, np.log10(2 / 3), ND)
        logchi = np.log10(chi)
        dchi = logchi[1] - logchi[0]
        PH = np.zeros((chi.shape[0], B.shape[0]), dtype=np.float64)
        for i, x in enumerate(chi):
            for j, b in enumerate(B):
                print("\r{:4.0f} / {:4.0f}; {:4.0f} / {:4.0f}  ({:5.1f}%) "
                    .format(
                        i + 1, chi.shape[0],
                        j + 1, B.shape[0],
                        100 * (i * B.shape[0] + j + 1) / (chi.shape[0] * B.shape[0])
                    ),
                    end="", flush=True
                )
                PH[i, j] = _single_atom_sigma_p(
                        B=b, chi=x, printflag=False, plotflag=False)
        print("")
        np.savetxt(B_dump, B)
        np.savetxt(chi_dump, chi)
        np.savetxt(data_dump, PH)

    ph_lim = (PH[0, -1] / abs(PH[0, -1])) * np.pi
    (pd.Plotter()
        .imshow(np.log10((PH - PHI0 - ph_lim) / (np.pi)),
            extent=[
                B.min() - dB / 2, B.max() + dB / 2,
                logchi.min() - dchi / 2, logchi.max() + dchi / 2
            ],
            origin="lower", aspect="auto",
            cmap=CMAP,
            #vmin=-1, vmax=+1
        )
        .colorbar()
        .set_xlabel("Field strength [G]")
        .set_ylabel("$\\log_{10} \\chi$")
        .set_clabel("$\\log_{10}[(\\Delta \\varphi - \\pi) / \\pi]$")
        #.set_clim(F.min(), 1)
        .grid(False)
        .savefig(
            outpath.joinpath(f"single-atom-2d_W={W0 / (2 * np.pi):.1f}MHz{ext}"))
        .close()
    )
    
    _single_atom_sigma_p(100, 0.01, printflag=False, plotflag=True)
    _single_atom_sigma_p(100, 0.67, printflag=False, plotflag=True)
    _single_atom_sigma_p(500, 0.01, printflag=False, plotflag=True)
    _single_atom_sigma_p(500, 0.67, printflag=False, plotflag=True)

if __name__ == "__main__":
    single_atom_sigma_p_2d()

