#![allow(dead_code)]
#![allow(non_snake_case)]
#![allow(non_camel_case_types)]
#![allow(clippy::needless_return)]

use std::{
    fs,
    path::PathBuf,
    f64::consts::PI,
};
use ndarray::{
    self as nd,
    array,
};
use num_complex::Complex64 as C64;
use sim::{
    loop_call,
    write_npz,
    rabi,
    systems::ground_prep::{
        self,
        NarrowlineState as GN,
        NarrowlineBasis,
    },
    hilbert::{
        Basis,
    },
};

const TITLE: &str = "ground-state prep";

fn do_pumping(
    W: f64,
    detuning: f64,
    aa: f64,
    bb: f64,
    th: f64,
    B: f64,
    outpath: Option<PathBuf>
) -> f64
{
    let t: nd::Array1<f64>
        = nd::Array::range(
            0.0,
            (1.0 / ground_prep::GAMMA) * 18.0,
            (2.0 * PI / W).min(1.0 / ground_prep::GAMMA) / 500.0,
        );

    let basis: NarrowlineBasis
        = NarrowlineBasis::from_pairs([
            (GN::g0, 2.0 * PI * ground_prep::zm(GN::g0, B)),
            (GN::g1, 2.0 * PI * ground_prep::zm(GN::g1, B)),
            (GN::n0, 2.0 * PI * (ground_prep::zm(GN::n0, B) - 3.0)),
            (GN::n1, 2.0 * PI * ground_prep::zm(GN::n1, B)),
            (GN::n2, 2.0 * PI * ground_prep::zm(GN::n2, B)),
            (GN::n3, 2.0 * PI * (ground_prep::zm(GN::n3, B) - 3.0)),
        ]);

    let psi0: nd::Array1<C64> = &(
        &basis.get_array(&GN::g0).unwrap()
        + &basis.get_array(&GN::g1).unwrap()
    ) / 2.0_f64.sqrt();
    // let psi0: nd::Array1<C64> = basis.get_array(&GN::g0).unwrap();
    let phi: nd::Array1<C64> = basis.get_array(&GN::g0).unwrap();

    let w: f64
        = basis.get_energy(&GN::n1).unwrap()
        - basis.get_energy(&GN::g0).unwrap()
        + (2.0 * PI * detuning);

    let H: nd::Array3<C64>
        = ground_prep::H_poincare(
            basis.clone(), w, W, aa, bb, th, t.view(), 0.0)
        .unwrap();
    let Y: nd::Array2<f64>
        = ground_prep::Y_lindblad(basis.clone());

    let rho: nd::Array3<C64> = rabi::lindblad_evolve_rk4(&psi0, &H, &Y, &t)
        .unwrap();
    // let rho: nd::Array3<C64> = rabi::liouville_evolve_rk4(&psi0, &H, &t)
    //     .unwrap();

    let P_psi0: nd::Array1<f64>
        = rho.axis_iter(nd::Axis(2))
        .map(|r| psi0.dot(&r).dot(&psi0).norm())
        .collect();
    let P_phi: nd::Array1<f64>
        = rho.axis_iter(nd::Axis(2))
        .map(|r| phi.dot(&r).dot(&phi).norm())
        .collect();
    let P_phi_max: f64
        = *P_phi.iter().max_by(|x, y| x.partial_cmp(y).unwrap()).unwrap();

    if let Some(dir) = outpath {
        write_npz!(
            dir.join(format!(
                "W={:.1}MHz_D={:+.3}MHz_aa={:+.3}pi_bb={:+.3}pi_th={:.3}pi_B={:.1}G.npz",
                W / (2.0 * PI),
                detuning,
                aa / PI,
                bb / PI,
                th / PI,
                B,
            ).as_str()),
            arrays: {
                "t" => &t,
                "rho" => &rho,
                "g0" => &basis.get_array(&GN::g0).unwrap(),
                "g1" => &basis.get_array(&GN::g1).unwrap(),
                "n0" => &basis.get_array(&GN::n0).unwrap(),
                "n1" => &basis.get_array(&GN::n1).unwrap(),
                "n2" => &basis.get_array(&GN::n2).unwrap(),
                "n3" => &basis.get_array(&GN::n3).unwrap(),
                "P_psi0" => &P_psi0,
                "P_phi" => &P_phi,
                "P_phi_max" => &array![P_phi_max],
                "detuning" => &array![detuning],
                "W" => &array![W / (2.0 * PI)],
                "aa" => &array![aa],
                "bb" => &array![bb],
                "th" => &array![th],
                "B" => &array![B],
                "H" => &H,
                "Y" => &Y,
            }
        );
    }

    return P_phi_max;
}

fn test_lindblad() {
    let basis: NarrowlineBasis
        = NarrowlineBasis::from_pairs([
            (GN::g0, 2.0 * PI * ground_prep::zm(GN::g0, 250.0)),
            (GN::g1, 2.0 * PI * ground_prep::zm(GN::g1, 250.0)),
            (GN::n0, 2.0 * PI * ground_prep::zm(GN::n0, 250.0)),
            (GN::n1, 2.0 * PI * ground_prep::zm(GN::n1, 250.0)),
            (GN::n2, 2.0 * PI * ground_prep::zm(GN::n2, 250.0)),
            (GN::n3, 2.0 * PI * ground_prep::zm(GN::n3, 250.0)),
        ]);
    let Y: nd::Array2<f64> = ground_prep::Y_lindblad(basis);
    write_npz!(
        PathBuf::from("output/lindblad_test_rs.npz"),
        arrays: { "Y" => &Y }
    );
}

fn single_test(outpath: PathBuf) {
    let config: ground_prep::Config = ground_prep::load_config(None);
    let detuning: f64 = 0.0; // MHz
    println!(
        "{}\n{}\n\
        Omega = {} MHz\n\
        Delta = {} MHz\n\
        B = {} G\n\
        aa = {} deg\n\
        bb = {} deg\n\
        th = {} deg\n\
        {1}",
        TITLE,
        "=".repeat(TITLE.len()),
        config.W / (2.0 * PI),
        detuning,
        config.B,
        config.aa * 180.0 / PI,
        config.bb * 180.0 / PI,
        config.th * 180.0 / PI,
    );
    do_pumping(
        config.W,
        detuning,
        config.aa,
        config.bb,
        config.th,
        config.B,
        Some(outpath)
    );
}

fn WB_test(outdir: PathBuf) {
    let config: ground_prep::Config = ground_prep::load_config(None);
    let detuning: f64 = 0.0; // MHz
    println!(
        "{}\n{}\n\
        Delta = {} MHz\n\
        aa = {} deg\n\
        bb = {} deg\n\
        th = {} deg\n\
        {1}",
        TITLE,
        "=".repeat(TITLE.len()),
        detuning,
        config.aa * 180.0 / PI,
        config.bb * 180.0 / PI,
        config.th * 180.0 / PI,
    );

    let outfile: PathBuf
        = outdir.join(format!(
            "W-B_D={:+.3}MHz_aa={:+.3}pi_bb={:+.3}pi_th={:.3}pi.npz",
            detuning,
            config.aa * 180.0 / PI,
            config.bb * 180.0 / PI,
            config.th * 180.0 / PI,
        ).as_str());

    let W: nd::Array1<f64> = nd::Array::linspace(0.05, 0.5, 50);
    let B: nd::Array1<f64> = nd::Array::linspace(0.0, 15.0, 50);

    let caller = |ijk: Vec<usize>| -> (f64,) {
        (
            do_pumping(
                W[ijk[0]] * (2.0 * PI),
                detuning,
                config.aa,
                config.bb,
                config.th,
                B[ijk[1]],
                None
            ),
        )
    };
    let (F,): (nd::ArrayD<f64>,) = loop_call!(
        caller => (F: f64,),
        vars: { W, B }
    );
    write_npz!(
        outfile,
        arrays: {
            "W" => &W,
            "B" => &B,
            "F" => &F,
        }
    );
}

fn main() {
    // test_lindblad();

    let outpath = PathBuf::from("output/ground_prep");
    if !outpath.is_dir() {
        fs::create_dir_all(outpath.as_path())
            .unwrap_or_else(|_| {
                panic!(
                    "Couldn't create directory {:?}",
                    outpath.to_str().unwrap()
                )
            });
        println!(":: mkdir -p {:?}", outpath.to_str().unwrap());
    }

    single_test(outpath);
    // WB_test(outpath);
}

