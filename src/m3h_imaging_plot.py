import numpy as np
import lib.plotdefs as pd
from pathlib import Path
import sys

infiles = (
    [Path(arg) for arg in sys.argv[1:]]
    if len(sys.argv[1:]) >= 1 else
    [(Path("output")
        .joinpath("m3h_imaging")
        .joinpath("W=0.2MHz_D=+0.000MHz_aa=-0.611pi_bb=+0.250pi_th=0.750pi_B=3.5G.npz")
    )]
)

for infile in infiles:
    print(infile)

    data = np.load(str(infile))
    t = data["t"]
    rho = data["rho"]
    score = data["score"][0]

    state_labels = [
        "$\\left| g_0 \\right\\rangle$",
        "$\\left| g_1 \\right\\rangle$",
        "$\\left| n_0 \\right\\rangle$",
        "$\\left| n_1 \\right\\rangle$",
        "$\\left| n_2 \\right\\rangle$",
        "$\\left| n_3 \\right\\rangle$",
    ]

    P = pd.Plotter()
    for k in range(rho.shape[0]):
    # for k in range(1, 2):
        (P
            .plot(t, rho[k, k, :].real, color=f"C{k % 10}", label=state_labels[k])
            # .plot(t, rho[k, k, :].imag, color=f"C{k % 10}", linestyle="--")
        )
    (P
        .ggrid()
        .legend(fontsize=5, loc=5)
        # .set_xlim(-0.5, 20.5)
        .set_xlabel("Time [$\\mu$s]")
        .set_ylabel("Probability")
        .set_title(f"Score = {score:.5f}")
        .savefig(infile.with_suffix(".png"))
        # .show()
        .close()
    )

