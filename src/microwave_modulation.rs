#![allow(dead_code)]
#![allow(non_snake_case)]
#![allow(non_camel_case_types)]
#![allow(clippy::needless_return)]

use std::{
    fs,
    path::PathBuf,
    f64::consts::PI,
};
use rand::{
    prelude as rnd,
    Rng,
};
use num_complex::Complex64 as C64;
use ndarray::{
    self as nd,
    array,
    concatenate,
    s,
};
use sim::{
    loop_call,
    write_npz,
    read_npz,
    rabi,
    systems::microwave_modulation::{
        self as micromod,
        MicroModState as MM,
        MicroModBasis,
    },
    hilbert::{
        Basis,
    },
    utils,
};

const TITLE: &str = "microwave modulation";

fn random_phases<R>(N: usize, rng: &mut R) -> nd::Array1<C64>
where R: ?Sized + Rng
{
    return (0..N)
        .map(|_| (C64::i() * 2.0 * PI * rng.gen::<f64>()).exp())
        .collect();
}

fn phase_from_spectrum(F: &nd::Array1<f64>, f: &nd::Array1<f64>)
    -> (nd::Array1<f64>, nd::Array1<f64>)
{
    let N: usize = F.len();
    let df: f64 = (f[1] - f[0]).abs();

    let mut rng: rnd::ThreadRng = rnd::thread_rng();
    let phases: nd::Array1<C64> = random_phases(N, &mut rng);
    let F_C64: nd::Array1<C64>
        = F.iter().zip(phases)
        .map(|(Fk, phk)| Fk * phk)
        .collect();
    let F_full: nd::Array1<C64>
        = concatenate!(
            nd::Axis(0),
            F_C64,
            F_C64.slice(s![1..;-1]).mapv(|Fk| Fk.conj())
        );

    let (phi, t): (nd::Array1<C64>, nd::Array1<f64>)
        = utils::do_ifft(&F_full, df);
    return (phi.mapv(|phk| phk.re), t);
}

fn do_micromod(
    W: f64,
    Wu: f64,
    Du: f64,
    Fphi2: &nd::Array1<f64>,
    f: &nd::Array1<f64>,
    k: usize,
    outdir: Option<PathBuf>,
) -> (f64, f64, f64, f64)
{
    let basis: MicroModBasis // energies in (us)^-1
        = MicroModBasis::from_pairs([
            (MM::g, 0.0),
            (MM::r, 0.0),
            (MM::s, 2.0 * PI * 10e3),
        ]);
    let g: nd::Array1<C64> = basis.get_array(&MM::g).unwrap();
    let r: nd::Array1<C64> = basis.get_array(&MM::r).unwrap();
    let s: nd::Array1<C64> = basis.get_array(&MM::s).unwrap();

    let t: nd::Array1<f64> = nd::Array1::range(
        0.0,
        2.0 * PI / W * 15.0 * 1e-6,
        2.0 * PI / W.max(Wu).max(Du) / 1000.0 * 1e-6,
    ); // s
    let phi: nd::Array1<f64>
        = utils::do_ifft_real_noise(Fphi2, f, &t);
    let t: nd::Array1<f64> = 1e6 * t; // us

    let H_gr: nd::Array3<C64>
        = micromod::H_poincare_trace( // |g> -> |r>; pure sigma^+
            basis.clone(),
            phi.view(),
            nd::Array1::from_elem(t.raw_dim(), W).view(),
            PI / 4.0, // |H>-|V> mixing angle wrt |H>
            PI / 2.0, // relative phase
            0.0, // k-vector angle wrt B-field
            t.view()
        ).unwrap();
    let H_rs: nd::Array3<C64>
        = micromod::H_poincare_trace( // |r> -> |s>; pure pi
            basis.clone(),
            (
                (
                    basis.get_energy(&MM::s).unwrap()
                    - basis.get_energy(&MM::r).unwrap()
                    + Du
                ) * &t
                - &phi
            ).view(),
            nd::Array1::from_elem(t.raw_dim(), Wu).view(),
            PI / 2.0, // |H>-|V> mixing angle wrt |H>
            0.0, // relative phase
            PI / 2.0, // k-vector angle wrt B-field
            t.view()
        ).unwrap();
    let H: nd::Array3<C64> = H_gr + H_rs;

    let psi: nd::Array2<C64>
        = rabi::schrodinger_evolve_rk4(&g, &H, &t).unwrap();

    let P_g: nd::Array1<f64>
        = g.dot(&psi).mapv(|a| a.norm().powi(2));
    let P_r: nd::Array1<f64>
        = r.dot(&psi).mapv(|a| a.norm().powi(2));
    let P_s: nd::Array1<f64>
        = s.dot(&psi).mapv(|a| a.norm().powi(2));

    let N: usize = t.len();
    let P_g_max: f64
        = *P_g.slice(s![2 * N / 3..]).iter()
            .max_by(utils::float_iter_cmp).unwrap();
    let P_g_min: f64
        = *P_g.slice(s![2 * N / 3..]).iter()
            .min_by(utils::float_iter_cmp).unwrap();
    let P_g_contrast: f64
        = (P_g_max - P_g_min) / (P_g_max + P_g_min);
    let P_r_max: f64
        = *P_r.slice(s![2 * N / 3..]).iter()
            .max_by(utils::float_iter_cmp).unwrap();
    let P_s_max: f64
        = *P_s.slice(s![2 * N / 3..]).iter()
            .max_by(utils::float_iter_cmp).unwrap();

    if let Some(dir) = outdir {
        write_npz!(
            dir.join(
                format!(
                    "W={:.3}MHz_Wu={:.3}MHz_Du={:.3}MHz_k={}.npz",
                    W / (2.0 * PI),
                    Wu / (2.0 * PI),
                    Du / (2.0 * PI),
                    k,
                ).as_str()
            ),
            arrays: {
                "t" => &t,
                "phi" => &phi,
                "W" => &array![W / (2.0 * PI)],
                "Wu" => &array![Wu / (2.0 * PI)],
                "Du" => &array![Du / (2.0 * PI)],
                "psi" => &psi,
                "g" => &basis.get_array(&MM::g).unwrap(),
                "r" => &basis.get_array(&MM::r).unwrap(),
                "s" => &basis.get_array(&MM::s).unwrap(),
                "P_g" => &P_g,
                "P_g_max" => &array![P_g_max],
                "P_g_min" => &array![P_g_min],
                "P_g_contrast" => &array![P_g_contrast],
                "P_r" => &P_r,
                "P_r_max" => &array![P_r_max],
                "P_s" => &P_s,
                "P_s_max" => &array![P_s_max],
            }
        );
    }

    return (
        P_g_contrast,
        P_r_max,
        P_s_max,
        P_g_contrast * P_r_max - P_s_max,
    );
}

fn WD_test(
    W: f64,
    Fphi2: &nd::Array1<f64>,
    f: &nd::Array1<f64>,
    N: usize,
    ND: usize,
    outdir: PathBuf
) {
    let Wu: nd::Array1<f64> = nd::Array::linspace(0.0, 0.250, ND);
    let Du: nd::Array1<f64> = nd::Array::linspace(0.0, 8.000, ND);
    let trials: nd::Array1<usize> = (0..N).collect();

    let caller = |Q: Vec<usize>| -> (f64, f64, f64, f64) {
        do_micromod(
            W,
            2.0 * PI * Wu[Q[0]],
            2.0 * PI * Du[Q[1]],
            Fphi2,
            f,
            trials[Q[2]],
            // None,
            Some(outdir.clone()),
        )
    };
    let (P_g_contrast, P_r_max, P_s_max, fom)
        : (nd::ArrayD<f64>, nd::ArrayD<f64>, nd::ArrayD<f64>, nd::ArrayD<f64>)
        = loop_call!(
            caller
                => (P_g_contrast: f64, P_r_max: f64, P_s_max: f64, fom: f64,),
            vars: { Wu, Du, trials }
        );
    write_npz!(
        outdir.join("WuDu.npz"),
        arrays: {
            "Wu" => &Wu,
            "Du" => &Du,
            "P_g_contrast" => &P_g_contrast.mean_axis(nd::Axis(2)).unwrap(),
            "P_g_contrast_err" => &P_g_contrast.std_axis(nd::Axis(2), 0.0),
            "P_r_max" => &P_r_max.mean_axis(nd::Axis(2)).unwrap(),
            "P_r_max_err" => &P_r_max.std_axis(nd::Axis(2), 0.0),
            "P_s_max" => &P_s_max.mean_axis(nd::Axis(2)).unwrap(),
            "P_s_max_err" => &P_s_max.std_axis(nd::Axis(2), 0.0),
            "fom" => &fom.mean_axis(nd::Axis(2)).unwrap(),
            "fom_err" => &fom.std_axis(nd::Axis(2), 0.0),
        }
    );
}

fn main() {
    let outpath = PathBuf::from("output/microwave_modulation");
    if !outpath.is_dir() {
        fs::create_dir_all(outpath.as_path())
            .unwrap_or_else(|_| {
                panic!(
                    "Couldn't create directory {:?}",
                    outpath.to_str().unwrap()
                )
            });
        println!(":: mkdir -p {:?}", outpath.to_str().unwrap());
    }

    let config: micromod::Config = micromod::load_config(None);

    println!(
        "{}\n{}\n\
        W = {} MHz\n\
        Wu = {} MHz\n\
        Du = {} MHz\n\
        spectrum file = {}\n\
        {1}",
        TITLE,
        "=".repeat(TITLE.len()),
        config.W / (2.0 * PI),
        config.Wu / (2.0 * PI),
        config.Du / (2.0 * PI),
        config.spec_file.to_str().unwrap(),
    );

    let (Fphi, f): (nd::Array1<f64>, nd::Array1<f64>)
        = read_npz!(
            config.spec_file,
            arrays: { "Fphi.npy", "f.npy" }
        );
    let Fphi2: nd::Array1<f64> = Fphi.mapv(|F| F.powi(2));

    // for k in 0..config.N {
    //     do_micromod(
    //         config.W,
    //         config.Wu,
    //         config.Du,
    //         &Fphi2,
    //         &f,
    //         k,
    //         Some(outpath.clone())
    //     );
    // }
    WD_test(config.W, &Fphi2, &f, config.N, config.ND, outpath);
}

