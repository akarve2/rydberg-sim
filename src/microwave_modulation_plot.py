import numpy as np
import lib.plotdefs as pd
from pathlib import Path
import sys

infiles = (
    [Path(arg) for arg in sys.argv[1:]]
    if len(sys.argv[1:]) >= 1 else
    [(Path("output")
        .joinpath("microwave_modulation")
        .joinpath("W=6.0MHz_Wu=400.0MHz_D=12500.0MHz.npz")
    )]
)

for infile in infiles:
    print(infile)

    data = np.load(str(infile))
    t = data["t"]
    psi = data["psi"]

    state_labels = [
        "$\\left| g \\right\\rangle$",
        "$\\left| r \\right\\rangle$",
        "$\\left| s \\right\\rangle$",
    ]

    P = pd.Plotter()
    for k in range(psi.shape[0]):
        (P
            .plot(
                t, np.abs(psi[k, :])**2,
                color=f"C{k % 10}", label=state_labels[k]
            )
        )
    (P
        .ggrid()
        .legend(fontsize=5, loc=5)
        # .set_xlim(0.0, 10.0)
        .set_xlabel("Time [$\\mu$s]")
        .set_ylabel("Probability")
        .savefig(infile.with_suffix(".png"))
        # .show()
        .close()
    )

