import numpy as np
import lib.plotdefs as pd
from pathlib import Path

N = 10
outdir = Path("output").joinpath("microwave_modulation_single")

infiles = [
    outdir.joinpath(f"W=6.0MHz_Wu=400.0MHz_D=12500.0MHz_k={k:.0f}.npz")
    for k in range(N)
]

data_init = np.load(str(infiles[0]))
t = data_init["t"]
P_G = np.zeros((t.shape[0], N), dtype=np.float64)
P_R = np.zeros((t.shape[0], N), dtype=np.float64)
P_S = np.zeros((t.shape[0], N), dtype=np.float64)

for k, infile in enumerate(infiles):
    print(infile)

    data = np.load(str(infile))
    P_G[:, k] = data["P_g"]
    P_R[:, k] = data["P_r"]
    P_S[:, k] = data["P_s"]

P_g = P_G.mean(axis=1)
P_g_err = P_G.std(axis=1)
P_r = P_R.mean(axis=1)
P_r_err = P_R.std(axis=1)
P_s = P_S.mean(axis=1)
P_s_err = P_S.std(axis=1)

P = pd.Plotter()
for k, (label, p, p_err) in enumerate([
    ("g", P_g, P_g_err),
    ("r", P_r, P_r_err),
    ("s", P_s, P_s_err),

    state_labels = [
        "$\\left| g \\right\\rangle$",
        "$\\left| r \\right\\rangle$",
        "$\\left| s \\right\\rangle$",
    ]

    P = pd.Plotter()
    for k in range(psi.shape[0]):
        (P
            .plot(
                t, np.abs(psi[k, :])**2,
                color=f"C{k % 10}", label=state_labels[k]
            )
        )
    (P
        .ggrid()
        .legend(fontsize=5, loc=5)
        # .set_xlim(0.0, 10.0)
        .set_xlabel("Time [$\\mu$s]")
        .set_ylabel("Probability")
        .savefig(infile.with_suffix(".png"))
        # .show()
        .close()
    )

