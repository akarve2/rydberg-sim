#![allow(dead_code, non_snake_case, non_camel_case_types)]
#![allow(unused_variables, unused_imports)]
#![allow(clippy::needless_return)]

use std::{
    collections::HashMap,
    fs,
    path::PathBuf,
    f64::consts::PI,
};
use ndarray::{
    self as nd,
    s,
};
use ndarray_npy::{
    // self as npy,
    NpzWriter,
};
use itertools::Itertools;
use num_complex::Complex64 as C64;
use sim::{
    // write_npz,
    rabi,
    systems::rydberg_generic::{
        self as rydberg,
        U0,
        RydbergState as GR,
        RydbergBasis,
        RydbergProdBasis,
        CouplingType,
    },
    hilbert::{
        // BasisState,
        Basis,
        ProdBasis,
    },
};

fn state_label(state: &[GR]) -> String {
    return state.iter()
        .map(|s| {
            match *s {
                GR::g => 'g',
                // GR::e => 'e',
                GR::r => 'r',
            }
        })
        .collect();
}

fn doit_single(
    basis: &RydbergBasis,
    pbasis: &RydbergProdBasis,
    ss: Vec<GR>,
    W: f64,
    t: &nd::Array1<f64>,
) -> (String, nd::Array2<C64>)
{
    let H: nd::Array3<C64>
        = rydberg::H_poincare(
            basis.clone(),
            0.0,
            W,
            PI / 4.0,
            PI / 2.0,
            0.0,
            t.view(),
            0.0,
        ).unwrap();
    let HH: nd::Array3<C64>
        = rydberg::gen_multiatom(
            CouplingType::AllToAll,
            pbasis.clone(),
            H.view(),
            U0,
        ).unwrap();
    let psi0: nd::Array1<C64> = pbasis.get_array(&ss).unwrap();
    let psi: nd::Array2<C64>
        = rabi::schrodinger_evolve_rk4(&psi0, &HH, t).unwrap();

    return (state_label(&ss), psi);
}

fn doit(outfile: PathBuf) {
    let W: f64 = 2.0 * PI * 1.0; // Rabi frequency; us^-1
    let T: f64 = 2.0 * PI / W; // Rabi period; us
    let t: nd::Array1<f64> = nd::Array::range(0.0, 2.0 * T, T / 1000.0);

    let basis: RydbergBasis
        = RydbergBasis::from_pairs([
            (GR::g, 0.0),
            // (GR::e, 0.0),
            (GR::r, 0.0),
        ]);
    let pbasis: RydbergProdBasis = basis.to_multiatom(2).unwrap();

    let traces: HashMap<String, nd::Array2<C64>>
        = (0..2)
        // .map(|_| [GR::g, GR::e, GR::r].into_iter())
        .map(|_| [GR::g, GR::r].into_iter())
        .multi_cartesian_product()
        .map(|ss| doit_single(&basis, &pbasis, ss, W, &t))
        .collect();

    let mut npz = NpzWriter::new(fs::File::create(outfile).unwrap());
    npz.add_array("t", &t).unwrap();
    for trace in traces.into_iter() {
        npz.add_array(trace.0, &trace.1).unwrap();
    }
    npz.finish().unwrap();
}

fn main() {
    let outpath = PathBuf::from("output/rydberg-generic");
    if !outpath.is_dir() {
        fs::create_dir_all(outpath.as_path())
            .unwrap_or_else(|_| {
                panic!(
                    "Couldn't create directory {:?}",
                    outpath.to_str().unwrap()
                )
            });
        println!(":: mkdir -p {:?}", outpath.to_str().unwrap());
    }

    doit(outpath.join("traces.npz"));
}

