import sympy as sy
from sympy.physics.wigner import wigner_3j

q = 1

trans_w3j = lambda F0, mF0, F1, mF1: (
    sy.S(abs(sy.sqrt(2 * F0 + 1)
        * wigner_3j(F1, 1, F0, mF1, mF0 - mF1, -mF0)))
)

def print_table(name: str, F0, F1, mF0p, mF1p):
    print(name)
    print(len(name) * "=")
    print("F0          mF0         F1          mF1         W         ")
    print("----------  ----------  ----------  ----------  ----------")
    fmt = "  ".join("{:10}" for k in range(5))
    for mF0 in [F0 - k for k in range(int(2 * F0 + 1))]:
        for mF1 in [F1 - k for k in range(int(2 * F1 + 1))]:
            if abs(mF1 - mF0) <= 1:
                W_top = trans_w3j(F0, mF0, F1, mF1)
                W_bot = trans_w3j(F0, mF0p, F1, mF1p)
                W = (W_top / W_bot).simplify()
                print(fmt.format(str(F0), str(mF0), str(F1), str(mF1), str(W)))

print_table("Rydberg", sy.S(1) / 2, sy.S(3) / 2, sy.S(1) / 2, sy.S(3) / 2)
print("")
print_table("Clock", sy.S(1) / 2, sy.S(1) / 2, -sy.S(1) / 2, sy.S(1) / 2)

